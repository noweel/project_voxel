﻿using UnityEngine;
using System.Collections;

public class TransparentObj : MonoBehaviour
{

    Material Omat;
    Material Tmat;
    Shader Trshd;
    void Start()
    {
       Trshd= Resources.Load("Shaders/Mtrans", typeof(Shader)) as Shader;
        Tmat = new Material(Trshd);
        // 반투명 임시 마테리얼 생성
    }

    // 반투명화
    public void TransparentU()
    {
        Omat = this.GetComponent<Renderer>().sharedMaterial;  // 기존 쉐이더 임시저장
        Omat.GetTexture("_MainTex"); // 텍스쳐 정보 뺴오기
        Tmat.SetTexture("_MainTex", Omat.GetTexture("_MainTex")); // 텍스쳐 정보 입히기
        this.GetComponent<Renderer>().sharedMaterial = Tmat; // 반투명 마테리얼로 교체
    }


    // 원래대로
    public void RevealU()
    {
        this.GetComponent<Renderer>().sharedMaterial = Omat;
    }
}
