﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CamSceneChange : MonoBehaviour {

    public string TagName;
    public GameObject TargetCamera;
    public GameObject CurrentCamera;
    Camera CurrentCam;
    Quaternion CurrentCamRot;

    public UIMovement[] SetUI;
	// Use this for initialization
	void Start () {
        CurrentCam = CurrentCamera.GetComponent<Camera>();

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if ((Input.touchCount>0&&Input.GetTouch(0).phase==TouchPhase.Ended&&Input.GetTouch(0).tapCount==2))
        {
            Ray ray = CurrentCam.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit rayhit;
            if (Physics.Raycast(ray, out rayhit, 100f))
            {
                if (rayhit.transform.CompareTag(TagName))
                {
                    CurrentCamRot=CurrentCam.transform.localRotation;
                    CurrentCam.transform.LookAt(rayhit.point);
                    ZoominObject(rayhit.point);
                } 
            }
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = CurrentCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayhit;
            if (Physics.Raycast(ray, out rayhit, 100f))
            {
                if (rayhit.transform.CompareTag(TagName))
                {
                    CurrentCamRot = CurrentCam.transform.localRotation;
                    CurrentCam.transform.LookAt(rayhit.point);
                    ZoominObject(rayhit.point);
                }
            }
        }
#endif
    }

    void ZoominObject(Vector3 vc)
    {
        StartCoroutine("Zoomed",vc);
    }

    IEnumerator Zoomed(Vector3 vc)
    {
        yield return new WaitForSeconds(0.2f);

        while (CurrentCam.fieldOfView>20f)
        {
            CurrentCam.fieldOfView = CurrentCam.fieldOfView - 1f;
            yield return new WaitForFixedUpdate();
        }
        CurrentCam.fieldOfView = 60f;
        CurrentCam.transform.localRotation= CurrentCamRot;
        TargetCamera.SetActive(true);
        CurrentCamera.SetActive(false);

        if (SetUI!=null)
        {
            for (int i = 0; i < SetUI.Length; i++)
            {
                SetUI[i].PlayMove(true);
            }
        }

    }

}
