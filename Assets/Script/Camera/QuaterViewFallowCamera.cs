﻿using UnityEngine;
using System.Collections;

public class QuaterViewFallowCamera : MonoBehaviour {

    public Transform TargetOBJ; // 메인 캐릭터
    public float Distance;
    public float Height;
    public float AngleY;
    public GameObject Cam; // rotate만 하는 카메라 오브젝트
    // Use this for initialization
    Camera thisCamera;
    public bool FocusTarget; // 포커싱 유무
    public bool ChaseTarget_See;

    Transform TargetTransform; // 실제 목표
    // Update is called once per frame
    void Start()
    {
        thisCamera = Cam.GetComponent<Camera>();
        TargetTransform = TargetOBJ;
    }
    void LateUpdate ()
    {
        if (FocusTarget)
        {
            CameraMove(TargetTransform);
        }
    }

    // 카메라 박스의 이동과 카메라의 회전
    public void CameraMove(Transform Target)
    {
        if (!Target) return;
        // float CamYrot=this.transform.eulerAngles.y;
        // float TarYrot =TargetOBJ.transform.eulerAngles.y;
        // float LerpYangle = Mathf.LerpAngle(TarYrot, CamYrot, 0f);
        this.transform.position = whereiscam(Target);
       Cam.transform.LookAt(Target); // 로테이트는 카메라가 함
    }

    // 타겟에서의 알맞은 카메라 위치를 구하는법
    Vector3 whereiscam(Transform Targeter)
    {
        Quaternion myQua = Quaternion.Euler(0f, AngleY, 0f);
        Vector3 Pos = Targeter.position;
        Pos -= myQua * Vector3.forward * Distance;
        return new Vector3(Pos.x, Targeter.transform.position.y + Height, Pos.z);
    }

    // 포커싱 대상을 바꾸는 메소드
    public void CameraMoveFocus(Transform TargetTrans)
    {
        FocusTarget = false;
        StopCoroutine("MoveTransCam");
        StartCoroutine("MoveTransCam",TargetTrans);
    }

    // 포커싱대상을 따라 카메라 워크를 진행하는 코루틴
    IEnumerator MoveTransCam(Transform TargetTrans)
    {
        Vector3 TmpVec = whereiscam(TargetTrans);
        Vector3 Tvector = (TmpVec - this.transform.position);
        float Tdist = Vector3.Distance(TmpVec,this.transform.position);
        float Ddist=Tdist+1f;
        while (Tdist<Ddist)
        {
            this.transform.Translate(Tvector*Time.deltaTime);
            if (ChaseTarget_See) Cam.transform.LookAt(TargetTrans);
            yield return new WaitForEndOfFrame();
            Ddist = Tdist;
            Tdist = Vector3.Distance(TmpVec, this.transform.position);
        }
        TargetTransform = TargetTrans;
        FocusTarget = true;
    }

    // 메인캐릭터로 카메라가 돌아가는 메소드
    public void ReturnDeltaPos()
    {
        TargetTransform = TargetOBJ;
        FocusTarget = false;
        StopCoroutine("MoveTransCam");
        StartCoroutine("MoveTransCam", TargetTransform);
    }

}
