﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CnControls;

public class CamTouchMove : MonoBehaviour {
    // 기본좌표 7,14,-15
    // 미니캠은1.5,8,-1.5
    // 목표방향 벡터는 목표지점 좌표-현재 좌표임 헤깔리지말자

    public Vector3[] TargetPos=new Vector3[5];
    public Camera MyCamBox;
    public bool isMain;
    [HideInInspector]
    public static bool CanControl;


	// Use this for initialization
	void Start ()
    {
        CanControl = true;
	}
	
	// Update is called once per frame
	void LateUpdate()
    {
     //   if (Camera.main.enabled == false) return;
     // 터치앤 드래그 카메라 컨트롤
        if (Input.touchCount==1 && CanControl)
        {
            int Tatgetposnum;
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Ray ray = MyCamBox.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit rayhit;
                if (Physics.Raycast(ray, out rayhit, 100f))
                {
                   //쓸지몰라 추가해둔 부분
                }
            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved&&Input.GetTouch(0).deltaPosition.sqrMagnitude>30)
            {               
                if (Input.GetTouch(0).deltaPosition.x<=0)
                {
                    if (Input.GetTouch(0).deltaPosition.y <= 0) Tatgetposnum = 1;
                    else Tatgetposnum = 3;
                }
                else
                {
                    if (Input.GetTouch(0).deltaPosition.y <= 0) Tatgetposnum = 0;
                    else Tatgetposnum = 2;
                }
                Vector3 vector = TargetPos[Tatgetposnum] - this.transform.position;
                Move(vector, TargetPos[Tatgetposnum]);
            }
        }
#if UNITY_EDITOR  //에디터상 화면이동 테스트를 위한 스크립트
        if (Input.GetMouseButtonDown(0))
        {
            int Tatgetposnum;
            //Debug.Log(Input.mousePosition);

            Ray ray = MyCamBox.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayhit;
            if (!Physics.Raycast(ray, out rayhit, 100f)||CanControl)
            {
                if ((Input.mousePosition.x > (Screen.width / 10) * 4 && Input.mousePosition.x < (Screen.width / 10) * 6) &&
                    (Input.mousePosition.y > (Screen.height / 10) * 4 && Input.mousePosition.y < (Screen.height / 10) * 6))
                {
                    Tatgetposnum = 4;
                }
                else
                {
                    if (Input.mousePosition.x > Screen.width / 2)
                    {
                        if (Input.mousePosition.y > Screen.height / 2) Tatgetposnum = 1;
                        else Tatgetposnum = 3;
                    }
                    else
                    {
                        if (Input.mousePosition.y > Screen.height / 2) Tatgetposnum = 0;
                        else Tatgetposnum = 2;
                    }
                }

                Vector3 vector = TargetPos[Tatgetposnum] - this.transform.position;
                Move(vector, TargetPos[Tatgetposnum]);
            }
       
        }
#endif
    }

    Vector3 destpos = new Vector3();

    // pos(목표) originpos(원점)를 설정한후 카메라이동과 복귀를 실행
    void Move(Vector3 pos,Vector3 originpos)
    {
        destpos = originpos;
        StopCoroutine("SmoothMoveCamera"); //중간에 바로 시점이동가능하도로 중단
        StopCoroutine("GotoOriginPos"); // 새로운 무브 갱신시 복귀도 취소하고 새로 설정
        StartCoroutine("SmoothMoveCamera",pos);
        StartCoroutine("GotoOriginPos");
    }

    // 부드러운 카메라이동
    IEnumerator SmoothMoveCamera(Vector3 pos)
    {
        float dist=1f;
        while (dist>0.2f)
        {
            dist = Vector3.Distance(destpos, this.transform.localPosition);
            this.transform.Translate(pos*Time.deltaTime*(dist/4f));
            yield return new WaitForFixedUpdate();
        }
    }

    // 이동이후 복귀를 미리 설정
    IEnumerator GotoOriginPos()
    {
        yield return new WaitForSeconds(12f);
        float dist = 1f;
        Vector3 posi = TargetPos[4] - this.transform.localPosition;
        while (dist > 0.2f)
        {
            dist = Vector3.Distance(TargetPos[4], this.transform.localPosition);
            this.transform.Translate(posi * Time.deltaTime * (dist / 4f));
            yield return new WaitForFixedUpdate();
        }
    }

}
