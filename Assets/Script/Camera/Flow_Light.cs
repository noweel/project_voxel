﻿using UnityEngine;
using System.Collections;

public class Flow_Light : MonoBehaviour {

    public Transform Light_Trans;
    public float How_many_Minite_inGame_of_Day;
    public float Light_movement_Second;
    [Range(0,80)]
    public int Max_Light_angle;
    public bool isRunning;

    float Total_Seconds;
    float oneSec_moved_angle;
    float timecounter;
    // Use this for initialization
    public Skybox skybox;
    public Material[] Sky;
    void Start ()
    {
        isRunning = true;
        Light_Trans.transform.localEulerAngles = new Vector3(0f,0f,-(float)Max_Light_angle);
        StartCoroutine("DayLighter");       
    }

    IEnumerator DayLighter()
    {
        Total_Seconds = How_many_Minite_inGame_of_Day * 60;
        oneSec_moved_angle =(float)(2*Max_Light_angle)/ (float)Total_Seconds;
        Debug.Log(oneSec_moved_angle);
        timecounter = 0;
        while (timecounter < Total_Seconds)
        {
            yield return new WaitForSeconds(1f * Light_movement_Second);
            if (isRunning)
            {
                timecounter=timecounter+(1f*Light_movement_Second);
                Vector3 Eangles=Light_Trans.transform.localEulerAngles;
                Eangles.z += oneSec_moved_angle;
                Light_Trans.transform.localEulerAngles = Eangles;
                if (timecounter<Total_Seconds*0.2f)
                {
                    skybox.material = Sky[0];
                }
                else if (timecounter < Total_Seconds * 0.3f)
                {
                    skybox.material = Sky[1];
                }
                else if (timecounter < Total_Seconds * 0.7f)
                {
                    skybox.material = Sky[2];
                }
                else if (timecounter < Total_Seconds * 0.8f)
                {
                    skybox.material = Sky[3];
                }
                else if (timecounter < Total_Seconds)
                {
                    skybox.material = Sky[0];
                }

            }
        }
        Tomorrow();
    }

    public void Change_Var(int day_hour, int angle)
    {
        Total_Seconds = day_hour * 60 * 60-timecounter;
        oneSec_moved_angle = (2 * angle)/Total_Seconds;
    }

    public void Tomorrow()
    {
        Light_Trans.transform.localEulerAngles = new Vector3(0f, 0f, -(float)Max_Light_angle);
        StopCoroutine("DayLighter");
        StartCoroutine("DayLighter");
    }


}
