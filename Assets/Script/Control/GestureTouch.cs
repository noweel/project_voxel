﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GestureTouch : MonoBehaviour {

    bool isStart;
    bool isSecondCheck;
    bool isDoneCheck;
    bool isMatched;

    public Text tex;
	// Use this for initialization
	void Start () {
	
	}

    Vector2 TouchPos;
    Vector2 Deltapos;
    Vector2 OriginPos;
    int mypos;
	// Update is called once per frame
	void LateUpdate()
    {
        //터치2개체크->터치위치체크후 기준 포지션설정->
        if (Input.touchCount<=1)
        {
            isStart = isSecondCheck = isDoneCheck = isMatched = false;
        }

        if (Input.touchCount != 2) return;
        if (Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(1).phase == TouchPhase.Began && isStart == false)
        {
            if (Input.GetTouch(0).position.y > Screen.height * 0.75f && Input.GetTouch(1).position.y > Screen.height * 0.75f )
            {
                isStart = true;
                tex.text = Screen.height.ToString() + "____" + Input.GetTouch(0).position.y.ToString()+" 그으려고?";
            }
        }

        if (Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved&&isStart==true)
        {
            if ((Input.GetTouch(0).deltaPosition.normalized.y >= -1 && Input.GetTouch(0).deltaPosition.normalized.y <= -0.8f) && (
                Input.GetTouch(1).deltaPosition.normalized.y >= -1 && Input.GetTouch(1).deltaPosition.normalized.y <= -0.8f))
            {
                isDoneCheck = true;
                tex.text = "긋는중";
            }
            if (Input.GetTouch(0).position.y < Screen.height * 0.25f && Input.GetTouch(1).position.y < Screen.height * 0.25f && isStart == true && isDoneCheck == true)
            {
                tex.text = " 그었네";
                if (this.GetComponent<Igesture>() != null)
                {
                    this.GetComponent<Igesture>().Gaction();
                }
                isStart = false;
                isDoneCheck = false;
            }
        }


    }

    void CheckPos()
    {
        TouchPos = Input.GetTouch(0).position;
        if (TouchPos.x<=Screen.width/2)
        {
            if (TouchPos.y >= Screen.height / 2) mypos = 0;
            else mypos = 2;
        }
        else
        {
            if (TouchPos.y >= Screen.height / 2)  mypos = 1;
            else mypos = 3;
        }        
    }

}
