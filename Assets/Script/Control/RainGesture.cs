﻿using UnityEngine;
using System.Collections;

public class RainGesture : MonoBehaviour,Igesture {

    public ParticleSystem RainParticle;
    public void Gaction()
    {
        RainParticle.Play();
        Invoke("Stoprainning", 15f);
    }


    void Stoprainning()
    {
        RainParticle.Stop();
    }

}
