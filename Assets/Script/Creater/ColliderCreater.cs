﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ColliderCreater : MonoBehaviour {



    public void SetColliders()
    {
        Renderer[] tmptrans = this.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < tmptrans.Length; i++)
        {
            if (tmptrans[i].gameObject.GetComponent<BoxCollider>() == null)
            {
                tmptrans[i].gameObject.AddComponent<BoxCollider>();
            }
        }
    }

    public void destColliders()
    {
        BoxCollider[] tmptrans = this.GetComponentsInChildren<BoxCollider>();
        for (int i = 0; i < tmptrans.Length; i++)
        {
            DestroyImmediate(tmptrans[i]); //에디터상에서 컴포넌트는 즉시파괴 메소드만 적용됨
        }
    }

    public void ScriptAdd()
    {
        BoxCollider[] tmptrans = this.GetComponentsInChildren<BoxCollider>();
        for (int i = 0; i < tmptrans.Length; i++)
        {
            tmptrans[i].gameObject.AddComponent<TransparentObj>();
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ColliderCreater))]
    public class COCeditor : Editor
    {
        ColliderCreater Coc;
        bool MaterialSetting;
        void OnEnable()
        {
            Coc = target as ColliderCreater;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;
            base.OnInspectorGUI();
            if (GUILayout.Button("COllider Create"))
            {
                Coc.SetColliders(); 
            }
            if (GUILayout.Button("COllider Destroy"))
            {
                Coc.destColliders(); 
            }
            if (GUILayout.Button("Script"))
            {
                Coc.ScriptAdd();
            }
            EditorGUILayout.EndVertical();
        }
    }
#endif
}
