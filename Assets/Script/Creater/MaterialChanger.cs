﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MaterialChanger : MonoBehaviour
{

    public string TagName;
    public Material This_Material;

    GameObject[] Result_obj;


    public void Find_object()
    {
        Result_obj = GameObject.FindGameObjectsWithTag(TagName);
        for (int i = 0; i < Result_obj.Length; i++)
        {
            if (Result_obj[i].GetComponent<Renderer>())
            {
                Result_obj[i].GetComponent<Renderer>().material = This_Material;
            }
        }
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(MaterialChanger))]
    public class MaterialChangereditor : Editor
    {
        MaterialChanger Mch;
        bool MaterialSetting;
        void OnEnable()
        {
            Mch = target as MaterialChanger;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;
            base.OnInspectorGUI();
            if (GUILayout.Button("COllider Create"))
            {
                Mch.Find_object(); //유니티상에서 함수실행
            }
            EditorGUILayout.EndVertical();
        }
    }
#endif
}
