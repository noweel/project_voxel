﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class UnderB_Gen : MonoBehaviour {

    public GameObject Target_ParentObj;
    public Transform Parent_transform;
    public float block_tum;
    public Object ublock_obj;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ublockgenerator()
    {
        BoxCollider[] targetArray =Target_ParentObj.GetComponentsInChildren<BoxCollider>();
        for (int i = 0; i < targetArray.Length; i++)
        {
            Vector3 pos = targetArray[i].transform.position;
            pos.y -= block_tum;
            GameObject tmp = Instantiate(ublock_obj, pos, Quaternion.identity) as GameObject;
            tmp.transform.SetParent(Parent_transform);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(UnderB_Gen))]
    public class UnderB_GenEditor : Editor
    {
        UnderB_Gen Ubg;
        bool MaterialSetting;
        void OnEnable()
        {
            Ubg = target as UnderB_Gen;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;
            base.OnInspectorGUI();
            if (GUILayout.Button("Under_block Create"))
            {
                Ubg.ublockgenerator();
            }
            EditorGUILayout.EndVertical();
        }
    }
#endif
}

