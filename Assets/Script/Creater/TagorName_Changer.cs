﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TagorName_Changer : MonoBehaviour {

    public string Block_Tagname;
    public string Block_Change_Tage_Name;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ChangeObjectName()
    {
        if (Block_Tagname!=null)
        {
            GameObject[] GA=GameObject.FindGameObjectsWithTag(Block_Tagname);
            for (int i = 0; i < GA.Length; i++)
            {
                GA[i].name = Block_Tagname+i;
            }
        }
    }

    public void ChangeTag()
    {
        if (Block_Tagname!=null&& Block_Change_Tage_Name!=null)
        {
            GameObject[] GA = GameObject.FindGameObjectsWithTag(Block_Tagname);
            for (int i = 0; i < GA.Length; i++)
            {
                GA[i].tag = Block_Change_Tage_Name;
            }
        }
    }

    public void UntaggedToTagging()
    {
        if(Block_Change_Tage_Name!=null)
        {
            Transform[] Ta = this.GetComponentsInChildren<Transform>();
            for (int i = 0; i < Ta.Length; i++)
            {
                if (Ta[i].CompareTag("Untagged"))
                {
                    Ta[i].tag = Block_Change_Tage_Name;
                }
            }
        }
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(TagorName_Changer))]
    public class TagorName_Changereditor : Editor
    {
        TagorName_Changer Tnc;
        bool MaterialSetting;
        void OnEnable()
        {
            Tnc = target as TagorName_Changer;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;
            base.OnInspectorGUI();
            if (GUILayout.Button("Tag to Name"))
            {
                Tnc.ChangeObjectName(); //유니티상에서 함수실행
            }
            if (GUILayout.Button("Re-tag"))
            {
                Tnc.ChangeTag(); //유니티상에서 함수실행
            }
            if (GUILayout.Button("NEW-Tag"))
            {
                Tnc.UntaggedToTagging(); //유니티상에서 함수실행
            }
            EditorGUILayout.EndVertical();
        }
    }
#endif
}

