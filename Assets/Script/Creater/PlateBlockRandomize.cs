﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlateBlockRandomize : MonoBehaviour {

    public Mesh mesh_obj;
    public Material[] _Material; //등록할 마테리얼 배열
    public int[] _PecentageMaterial; //마테리얼의 퍼센티지
    public Shader[] _Shader; // 등록 쉐이더
    public Color _Color; // 아웃라인 색상
    [Range(0f, 0.001f)]
    public float _OutLine; //아웃라인 굵기
    //public int _SelectMaterial;
    public int _SelectShader; //선택한 쉐이더 배열 번호
    Shader SelShd;
    


    Renderer[] Childmat;

    void Awake()
    {
        if (_Shader.Length!=0&&_SelectShader<=_Shader.Length)  // 쉐이더 등록이 되있고 선택한 쉐이더 번호가 맞으면
        {
            SelShd = _Shader[_SelectShader]; // 쉐이더 지정
            for (int i = 0; i < _Material.Length; i++) // 등록한 마테리얼에  접근하여 쉐이더 변경
            {
                _Material[i].shader = SelShd;
            }
        }

        
    }
	// Use this for initialization
	void Start ()
    {
        

	}

    public void SetMaterial()
    {
        /// 쉐이더 옵션 조절 
        ///아직안함 // 다른 스크립트로 구현해서.. ^^...
        
        Childmat = GetComponentsInChildren<Renderer>(); // 렌더러가있는 자식 검사
        int[] z = new int[_PecentageMaterial.Length]; // 각각의 마테리얼 확률의 가지수 만큼 배열

        for (int i = 0; i < Childmat.Length; i++)  /// 렌더러 자식들 순회공연 시작
        {
            if (mesh_obj!=null)
            {
                Childmat[i].GetComponent<MeshFilter>().sharedMesh = mesh_obj;
            }
            Childmat[i].tag = "Block";  // 태그를 블럭으로 해준다 실시
            for (int j = 0; j < z.Length; j++)  
            {
                z[j] = Random.Range(0, _PecentageMaterial[j]); //z배열에 각 마테리얼에 맞춰 각 수치에 맞춰 랜덤값을 뽑아냄(최대값은 확률수치 정수화) 
            }
            int aa = Mathf.Max(z);  //배열 z중에 가장큰 값을 뽑아냄

            for (int k = 0; k < z.Length; k++) // 다시 z배열을 순회하여
            {
                if (z[k].Equals(aa)) //z의 값과 최대값이 같으면 선택된 마테리얼 이니
                {
                    Childmat[i].material = _Material[k]; // 해당 마테리얼로 교체
                }

            }

        }
    }

}
// 유니티 에디터에서 보이도록 편집
#if UNITY_EDITOR
[CustomEditor(typeof(PlateBlockRandomize))]
public class PBReditor:Editor
{
    PlateBlockRandomize PBR;
    bool MaterialSetting;
    void OnEnable()
    {
        PBR = target as PlateBlockRandomize;               
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginVertical("Box");
        EditorGUI.indentLevel++;
        base.OnInspectorGUI();
        if (GUILayout.Button("Radomize Field Material"))
        {
            PBR.SetMaterial(); //유니티상에서 함수실행
        }
        EditorGUILayout.EndVertical();
    }
}
#endif
