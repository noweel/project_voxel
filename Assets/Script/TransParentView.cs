﻿using UnityEngine;
using System.Collections;

public class TransParentView : MonoBehaviour {

    
    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Block")|| coll.CompareTag("Under_block"))
        {
            coll.SendMessage("TransparentU");
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.CompareTag("Block")||coll.CompareTag("Under_block"))
        {
            coll.SendMessage("RevealU");
        }
    }

}
