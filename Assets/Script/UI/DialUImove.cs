﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialUImove : MonoBehaviour {
    // Ui가 따라다닐 대상 및 대상 카메라 없다면 화면 중앙을 기준으로 오버레이
    public Transform TargetTransform;
    public Camera myCam;

    // 애니메이션 처리 여부 및 ui버튼 최소 크기
    public bool DialupAnimate;
    public bool SizeChangeEffect;
    public bool DontSpinDial;
    [Range(0, 1f)]
    public float MinimumSize;

    // dial을 구성하는 버튼ui및 ui전체의 반지름
    public GameObject[] DialObject;
    public int Radius;

    // 움직이는 시간과 총 프레임(셋팅시 수치를 알맞게)
    public int MoveFrame;
    public float MoveTime;

    // 변수
    Vector3 CurrentPos;
    GameObject[] dialobj;
    Vector3[] GoalPos;
    float Angle;
    [HideInInspector]
    public bool Spinning = false;
    [HideInInspector]
    public bool DialStart = false;

    public GameObject[] SubWindows;

    public void Setup()
    {
        // dialUI 셋팅 (UI오브젝트 생성 및 위치 지정)
        if (DialObject.Length > 0&&dialobj==null)
        {
            Angle = (360f / DialObject.Length) / MoveFrame;
            dialobj = new GameObject[DialObject.Length];
            GoalPos = new Vector3[DialObject.Length * MoveFrame];
            for (int j = 0; j < DialObject.Length * MoveFrame; j++)
            {
                GoalPos[j] = new Vector3(Mathf.Sin(Angle * j * Mathf.Deg2Rad), Mathf.Cos(Angle * j * Mathf.Deg2Rad), 0f) * Radius;
                // 원하는 각도에 맞는 위치를 찾기 위해서는 Sin/Cos에 대한 이해가 필요..
                // 결론적으로 정규화(nomalize)한 x=sin y=cos가 원하는 각도에 맞는 좌표임. 거기에 원하는 반지름(distance)를 곱해주면 최종 위치를 구할 수 있음
                // Debug.Log(180*Mathf.Deg2Rad); = Pi값이 나옴
                // Debug.Log(Mathf.Cos(180 * Mathf.Deg2Rad)); // deg2rad를 통해 일단 각도를 라디안화해야 Sin/Cos값을 구해야함
            }
            for (int i = 0; i < DialObject.Length; i++)
            {
                dialobj[i] = Instantiate(DialObject[i], this.transform) as GameObject;
                dialobj[i].GetComponent<RectTransform>().localScale = Vector3.one;
                dialobj[i].GetComponent<RectTransform>().localPosition = Vector3.zero;
                dialobj[i].SetActive(false);
            }
        }
    }
	
	// Update is called once per frame
	void LateUpdate ()
    {       
        // 카메라와 타겟이 있다면 추적 시작
        if (myCam == null || TargetTransform == null) return;
        this.transform.position = myCam.WorldToScreenPoint(new Vector3(TargetTransform.position.x, TargetTransform.position.y + 0.5f, TargetTransform.position.z));
    }

    // 다이얼 시작
    public void SetPosition()
    {
        Setup(); // 셋업

        if (DialStart) return;
        for (int i = 0; i < DialObject.Length; i++)
        {
            dialobj[i].SetActive(true);
            if (DialupAnimate)
            {
                StartCoroutine(Dialupmove(dialobj[i].GetComponent<RectTransform>(), GoalPos[i*MoveFrame], false));
            }
            else dialobj[i].GetComponent<RectTransform>().localPosition = GoalPos[i * MoveFrame];
        }
        DialStart = true;
    }

    // 다이얼 접기
    public void ClearDial()
    {
        if (!DialStart) return;

        if (SubWindows.Length>0)
        {
            for (int k = 0; k < SubWindows.Length; k++)
            {
                if (!SubWindows[k].GetComponent<DialUImove>()) SubWindows[k].SetActive(false);
            }
        }

        for (int i = 0; i < DialObject.Length; i++)
        {
            if (DialupAnimate)
            {
                StartCoroutine(Dialupmove(dialobj[i].GetComponent<RectTransform>(), -GoalPos[i * MoveFrame], true));
            }
            else dialobj[i].SetActive(false);
        }
        DialStart = false;
    }

    // 다이얼 열고 닫고하는 코루틴
    IEnumerator Dialupmove(RectTransform Rect, Vector3 Pos, bool isClose)
    {
        int i = 1;
        Vector3 Startpos = Rect.localPosition;
        while (i <= MoveFrame)
        {
            Startpos += (Pos / MoveFrame);
            Rect.localPosition = Startpos;

            if (SizeChangeEffect)
            {
                if (isClose) Rect.localScale = Vector3.one * (1f - ((1f-MinimumSize)/MoveFrame) * i);
                else Rect.localScale = Vector3.one * (MinimumSize + ((1f - MinimumSize) / MoveFrame) * i);
            }
            i++;
            yield return new WaitForSeconds(MoveTime / MoveFrame);
        }
        if (isClose)Rect.gameObject.SetActive(false);

    }

    // 회전 메소드 변수값은 회전방향을 불타입으로 표현
    public void SpinMove(bool Dir_right)
    {
        if (Spinning||!DialStart||DontSpinDial) return;
        for (int i = 0; i < DialObject.Length; i++)
        {
            if (DialupAnimate)
            {
                StartCoroutine(DialSpin(dialobj[i].GetComponent<RectTransform>(), i * MoveFrame, Dir_right));
            }
        }
    }

    // 회전 코루틴
    IEnumerator DialSpin(RectTransform Rect, int cu, bool isRight)
    {
        int i = 0;
        Spinning = true;
        while (Mathf.Abs(i) < MoveFrame)
        {
            if (cu==0&&!isRight)
            {
                Rect.localPosition = GoalPos[GoalPos.Length-1];
                cu = GoalPos.Length - 1;
            }
            else Rect.localPosition = GoalPos[cu + i];
            if (isRight) i++;
            else i--;
            yield return new WaitForSeconds(MoveTime / MoveFrame);
        }
        Spinning = false;
    }
}
