﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DialButton : MonoBehaviour, IDragHandler
{
    public Transform Rightpos;
    public Transform Leftpos;

    public enum DialButtonType { STAT, EQUIP, IVEN, OPT, AI_OPT, PRTY, SOCIAL, RECALL, CRAFT, REST, DIG };
    public DialButtonType MyType;

    public GameObject[] SubWindowUI;

    public void SubWindowPop()
    {
        SubWindowUI = new GameObject[this.transform.parent.GetComponent<DialUImove>().SubWindows.Length];
        SubWindowUI = this.transform.parent.GetComponent<DialUImove>().SubWindows;
        UIpopupLacator UIloca;
        for (int i = 0; i < SubWindowUI.Length; i++)
        {
            if (i == (int)MyType)
            {
                SubWindowUI[i].SetActive(true);
                SubWindowUI[i].transform.SetParent(this.transform);
                UIloca = SubWindowUI[i].GetComponent<UIpopupLacator>();
                if (UIloca != null)
                {
                    UIloca.Leftpos = this.transform.parent.FindChild("Pos").GetChild(0).GetComponent<RectTransform>();
                    UIloca.Rightpos = this.transform.parent.FindChild("Pos").GetChild(1).GetComponent<RectTransform>();
                    UIloca.FindPos();
                }
            }
            else if (SubWindowUI[i].GetComponent<DialUImove>()==null)
            {
                SubWindowUI[i].SetActive(false);
                SubWindowUI[i].transform.SetParent(this.transform.parent);
            }
            else SubWindowUI[i].transform.SetParent(this.transform.parent);
        }

    }

    public void Spin_dialPOP()
    {
            this.transform.parent.parent.GetComponent<DialUIConnecter>().Open_Spin_Dial();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (MyType == DialButtonType.AI_OPT)
        {
            if (this.transform.localPosition.x>=0&&this.transform.localPosition.y >= 0)
            {
                if (eventData.delta.x>8|| eventData.delta.y < -8)
                {
                    this.transform.parent.GetComponent<DialUImove>().SpinMove(true);//right
                }
                else if (eventData.delta.x < -8 || eventData.delta.y >8)
                {
                    this.transform.parent.GetComponent<DialUImove>().SpinMove(false);//left
                }
            }
            else if (this.transform.localPosition.x > 0 && this.transform.localPosition.y < 0)
            {
                if (eventData.delta.x < -8 || eventData.delta.y < -8)
                {
                    this.transform.parent.GetComponent<DialUImove>().SpinMove(true);//right
                }
                else if (eventData.delta.x > 8 || eventData.delta.y > 8)
                {
                    this.transform.parent.GetComponent<DialUImove>().SpinMove(false);//left
                }
            }
            else if (this.transform.localPosition.x <= 0 && this.transform.localPosition.y <= 0)
            {
                if (eventData.delta.x < -8 || eventData.delta.y > 8)
                {
                    this.transform.parent.GetComponent<DialUImove>().SpinMove(true);//right
                }
                else if (eventData.delta.x >8 || eventData.delta.y <-8)
                {
                    this.transform.parent.GetComponent<DialUImove>().SpinMove(false);//left
                }
            }
            else if (this.transform.localPosition.x < 0 && this.transform.localPosition.y > 0)
            {
                if (eventData.delta.x > 8 || eventData.delta.y < -8)
                {
                    this.transform.parent.GetComponent<DialUImove>().SpinMove(true);//right
                }
                else if (eventData.delta.x < -8 || eventData.delta.y > 8)
                {
                    this.transform.parent.GetComponent<DialUImove>().SpinMove(false);//left
                }
            }
        }
    }
}
