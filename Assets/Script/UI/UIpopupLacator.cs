﻿using UnityEngine;
using System.Collections;

public class UIpopupLacator : MonoBehaviour {
    public RectTransform Rightpos;
    public RectTransform Leftpos;
     
    public void FindPos()
    {
        Vector3 Parentpos = this.transform.parent.GetComponent<RectTransform>().localPosition;
        Vector3 Grandparentpos = this.transform.parent.parent.GetComponent<RectTransform>().localPosition;
        if (Grandparentpos.x <= 0) this.transform.SetParent(Rightpos);
        else this.transform.SetParent(Leftpos);
        this.transform.localPosition = Vector3.zero;
    }
}
