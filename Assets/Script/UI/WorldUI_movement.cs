﻿using UnityEngine;
using System.Collections;

public class WorldUI_movement : MonoBehaviour {

    public GameObject my_Target;
    public Transform Cam;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
        this.transform.position =new Vector3(my_Target.transform.position.x, my_Target.transform.position.y+2, my_Target.transform.position.z);
        this.transform.LookAt(Cam.transform);
	}
}
