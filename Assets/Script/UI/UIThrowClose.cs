﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class UIThrowClose : MonoBehaviour, IDragHandler, IPointerClickHandler
{
    public float ThrowSensivility;
    Vector3 PreMousePos;
    public void OnDrag(PointerEventData eventData)
    {
#if UNITY_EDITOR
        if (Mathf.Abs(eventData.delta.x)> ThrowSensivility|| Mathf.Abs(eventData.delta.y) > ThrowSensivility)
        {
            this.gameObject.SetActive(false);
        }

#endif
        if (Input.touchCount == 0) return;
        if (Input.GetTouch(0).phase == TouchPhase.Moved && (Mathf.Abs(eventData.delta.x) > ThrowSensivility || Mathf.Abs(eventData.delta.y) > ThrowSensivility))
        {
            this.gameObject.SetActive(false);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }

    // Use this for initialization
    void Start () {
        if (ThrowSensivility<=0)
        {
            ThrowSensivility = 10;
        }
	}
}
