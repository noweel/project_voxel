﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIMovement : MonoBehaviour {

    public enum UIMOVE_TYPE {Direct,Curve};
    public enum UIMOVE_METHOD { Once, Suttle };

    public Vector3 StartPos;
    public Vector3 Goal_LocalPosition;


    public float Moving_Frame_or_Count;
    public float MoveTime;
    public bool SizeChangeEffect;
    public float MinimumSize;

    public bool MoveEndClose;
    public float Endwaittime;

    public bool isReturn;
    public float ReturnWaittime;


    void Start()
    {
        StartPos = GetComponent<RectTransform>().localPosition;
    }

    public void PlayMove(bool isGo)
    {
        Vector3 pos = (isGo) ? Goal_LocalPosition : StartPos;
        StartCoroutine(Movement(this.GetComponent<RectTransform>(),pos));
        if (isReturn&&!MoveEndClose) Invoke("AutoReturn", MoveTime + ReturnWaittime);
    }

    void AutoReturn()
    {
        StartCoroutine(Movement(this.GetComponent<RectTransform>(), StartPos));
    }

    IEnumerator Movement(RectTransform Rect, Vector3 Pos)
    {
        int i = 1;
        Vector3 Dpos = Rect.localPosition;
        Vector3 move=new Vector3();

        if (Pos.x > 0 && Dpos.x < 0) move.x = Pos.x - Dpos.x;
        else if (Pos.x > 0 && Dpos.x > 0) move.x = Mathf.Abs(Pos.x - Dpos.x);
        else if (Pos.x < 0 && Dpos.x > 0) move.x = Dpos.x - Pos.x;
        else move.x=Mathf.Abs(Pos.x + Dpos.x);

        if (Pos.y > 0 && Dpos.y < 0) move.y = Pos.y - Dpos.y;
        else if (Pos.y > 0 && Dpos.y > 0) move.y = Mathf.Abs(Pos.y - Dpos.y);
        else if (Pos.y < 0 && Dpos.y > 0) move.y = Dpos.y - Pos.y;
        else move.y = Mathf.Abs(Pos.y + Dpos.y);

        bool rightm = (Pos.x > Dpos.x) ? true : false;
        bool upm= (Pos.y > Dpos.y) ? true : false;

        while (i <= Moving_Frame_or_Count)
        {

            Dpos.x += (move / Moving_Frame_or_Count).x * ((rightm) ? 1f : -1f);
            Dpos.y += (move / Moving_Frame_or_Count).y * ((upm) ? 1f : -1f);

            Rect.localPosition = Dpos;

            if (SizeChangeEffect)
            {
                if (MoveEndClose) Rect.localScale = Vector3.one * (1f - ((1f - MinimumSize) / Moving_Frame_or_Count) * i);
                else Rect.localScale = Vector3.one * (MinimumSize + ((1f - MinimumSize) / Moving_Frame_or_Count) * i);
            }
            i++;
            yield return new WaitForSeconds(MoveTime / Moving_Frame_or_Count);
        }
        yield return new WaitForSeconds(Endwaittime);
        if (MoveEndClose) Rect.gameObject.SetActive(false);

    }
}
