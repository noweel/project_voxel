﻿using UnityEngine;
using System.Collections;


public class DialUIConnecter : MonoBehaviour {

    public DialUImove[] DialUI;
    public Camera Cam;
    RaycastHit touchhit;
    Transform Tpos;
    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1)
        {
            Ray ray = Cam.ScreenPointToRay(Input.GetTouch(0).position);
            if (Physics.Raycast(ray, out touchhit))
            {
                if (touchhit.transform.CompareTag("Player"))
                {
                    DialUI[1].ClearDial();
                    DialUI[2].ClearDial();
                    DialUI[3].ClearDial();
                    Tpos = touchhit.transform;
                    if (DialUI[0].DialStart) DialUI[0].ClearDial();
                    else DialUI[0].SetPosition();
                }
                else if (touchhit.transform.CompareTag("Character"))
                {
                    DialUI[0].ClearDial();
                    DialUI[2].ClearDial();
                    DialUI[3].ClearDial();
                    
                    DialUI[1].TargetTransform = Tpos= touchhit.transform;
                    if (DialUI[1].DialStart) DialUI[1].ClearDial();
                    else DialUI[1].SetPosition();
                }
                else if (touchhit.transform.CompareTag("DialOBJ"))
                {
                    DialUI[0].ClearDial();
                    DialUI[1].ClearDial();
                    DialUI[3].ClearDial();

                    DialUI[2].TargetTransform = Tpos = touchhit.transform;
                    if (DialUI[2].DialStart) DialUI[2].ClearDial();
                    else DialUI[2].SetPosition();
                }
            }
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out touchhit))
            {
                if (touchhit.transform.CompareTag("Player"))
                {
                    DialUI[1].ClearDial();
                    DialUI[2].ClearDial();
                    DialUI[3].ClearDial();
                    Tpos = touchhit.transform;

                    if (DialUI[0].DialStart) DialUI[0].ClearDial();
                    else DialUI[0].SetPosition();
                }
                else if (touchhit.transform.CompareTag("Character"))
                {
                    DialUI[0].ClearDial();
                    DialUI[2].ClearDial();
                    DialUI[3].ClearDial();
                    DialUI[1].TargetTransform = Tpos = touchhit.transform;
                    if (DialUI[1].DialStart) DialUI[1].ClearDial();
                    else DialUI[1].SetPosition();
                }
                else if (touchhit.transform.CompareTag("DialOBJ"))
                {
                    DialUI[0].ClearDial();
                    DialUI[1].ClearDial();
                    DialUI[3].ClearDial();
                    DialUI[2].TargetTransform = Tpos = touchhit.transform;
                    if (DialUI[2].DialStart) DialUI[2].ClearDial();
                    else DialUI[2].SetPosition();
                }
            }
        }
#endif
    }

    public void Open_Spin_Dial()
    {
        DialUI[0].ClearDial();
        DialUI[1].ClearDial();
        DialUI[2].ClearDial();
        DialUI[3].TargetTransform = Tpos;
        if (DialUI[3].DialStart) DialUI[3].ClearDial();
        else DialUI[3].SetPosition();
    }

}
