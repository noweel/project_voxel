﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class UITouchControl : MonoBehaviour, IPointerClickHandler, IDragHandler,IBeginDragHandler,IEndDragHandler
{
    bool isPop=false;
    bool isDraged = false;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (GetComponent<UIMovement>()!=null&&isPop==false)
        {
            GetComponent<UIMovement>().PlayMove(true);
            isPop = true;        
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Input.touchCount == 0) return;
        if (Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(0).deltaPosition.sqrMagnitude > 25 && Input.GetTouch(0).deltaPosition.x < 0)
        {
            isDraged = true;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (isDraged==true)
        {
            if (GetComponent<UIMovement>() != null && isPop == true)
            {
                GetComponent<UIMovement>().PlayMove(false);
                isPop = false;
                isDraged = false;
            }
        }

#if UNITY_EDITOR
        if (GetComponent<UIMovement>() != null && isPop == true)
        {
            GetComponent<UIMovement>().PlayMove(false);
            isPop = false;
        }
#endif
    }
}
