﻿using UnityEngine;
using System.Collections;
using CnControls;

public class Player_Movement : MonoBehaviour
{
    public float moveSpeed;
    public float MaxJumpPower;
    public float JumpSpeed;
    Animator anit;
    CharacterController PlayerCon;
    Vector3 moveDir;

    void Awake()
    {
        anit = this.GetComponent<Animator>();
        PlayerCon = this.GetComponent<CharacterController>();
    }
	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0f) return;
        PJump();
        PMove();
    }

    void PMove()
    {
        float h = CnInputManager.GetAxis("Horizontal");
        float v = CnInputManager.GetAxis("Vertical");

        //이동방향 벡터 동서남북
        moveDir = new Vector3(h, 0f, v);

        //동서남북 이동에 따른 애니메이션 제어
        if (moveDir.x != 0 || moveDir.z != 0)
        {
           // anit.SetBool("Run", true);
        }
        else
        {
           // anit.SetBool("Run", false);
        }

        //캐릭터의 주시방향설정
        if (moveDir != Vector3.zero)
        {
            transform.forward = moveDir.normalized;
        }

        // 대각선이동 h/v동시 입력에 따른 속도 증가 분 제어
        float speed = moveSpeed;
        if (h != 0 && v != 0)
        {
            float degree = Mathf.Cos(45 * Mathf.Deg2Rad);
            speed = speed * degree;
        }

        //이동방향에 따른 속도값 더하기
        moveDir *= speed;
        // 캐릭터 컨트롤러를 이용하여 이동시키기. //캐릭터컨트롤러는 무브추가 메소드만을 통해 이동한다 리지드바디 안먹는것도 특징이라 온콜리젼엔터엑시트스테이 전부 안먹는다.
        PlayerCon.Move(moveDir*Time.deltaTime);
    }

    void PJump()
    {
     //  if (anit.GetCurrentAnimatorStateInfo(0).IsName("Jump")) return;
        if (Input.GetButtonDown("Jumper"))
        {
            //   anit.SetTrigger("Jump");
            StopCoroutine("PlayerJumpCo");
            StartCoroutine("PlayerJumpCo");
        }
    }

    IEnumerator PlayerJumpCo()
    {
        float y_pos= this.transform.position.y;
        while (y_pos+MaxJumpPower>=this.transform.position.y)
        {
            PlayerCon.Move(Vector3.up * JumpSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    void OnControllerColliderHit(ControllerColliderHit coll)
    {
        if (coll.gameObject.CompareTag("MoveThing"))   this.transform.SetParent(coll.transform);
        else this.transform.SetParent(null);

    }

    public void FreezeGame()
    {
        Time.timeScale = 0f;
    }

    public void MeltGame()
    {
        Time.timeScale = 1f;
    }
}
