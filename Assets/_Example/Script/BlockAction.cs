﻿using UnityEngine;
using System.Collections;

public class BlockAction : MonoBehaviour {
    RaycastHit Hitray; //레이에 충돌된 블럭
    GameObject RiftingObject; //들어올린 오브젝트
    Ray ray; // 블럭검사용 레이
    GameObject Tmpobj; // 충돌검사간 비교를 위한 임시 오브젝트
    bool Rifting = false; // 들어올린 여부
    public Transform[] rayPos=new Transform[2]; //충돌검사용 레이의 위치정보
    public float Distance; // 블럭 충돌측정 거리
    public Transform RiftPosition; // 들어올렸을때 위치

    Material mat; // 충돌검사로 찾은 오브젝트의 마테리얼
    public float OutlineWidth; // 아웃라인 굵기
    public Color OutlineColor; // 아웃라인 색상

    public GameObject[] Tranparent; // 비전블럭의 종류
    int TranNum; //비전블럭 크기번호
    // Use this for initialization
    void Start () {
      Hitray = new RaycastHit();
      Tmpobj = new GameObject();
    }
	
	// Update is called once per frame
	void Update ()
    {
        // 움직이는 블럭 판단 (레이어마스크로 구분) // 들기상태가 아닐때만
        if (!Rifting)
        {
            CheckBlockCanMoved();
        }

        // 키입력과 들기상태가 아니라면 들기
        if (Input.GetKeyDown(KeyCode.Z) && Rifting.Equals(false))
        {
            if (Hitray.collider!=null)
            {
                Rifting = true;
                RiftingObject = Hitray.collider.gameObject;

                Rift();
                if (RiftingObject.CompareTag("Block10"))
                {
                    TranNum = 0;
                }
                else if (RiftingObject.CompareTag("Block05"))
                {
                    TranNum = 1;
                }
                Tranparent[TranNum].SetActive(true);
            }

        }
        // 들고있는 상태라면 내려놓기
        else if (Input.GetKeyDown(KeyCode.Z) && Rifting.Equals(true))
        {
            Rifting = false;
            Drop();
            Tranparent[TranNum].SetActive(false);
        }
    }

    void CheckBlockCanMoved()
    {
        // 레이 구하기
        Vector3 pos = rayPos[1].position - rayPos[0].position;
        ray = new Ray(rayPos[0].position,pos);

        //레이캐스트로 해당하는 레이어마스크만 검출하고 아웃라인표시
        if (Physics.Raycast(ray, out Hitray,2f,LayerMask.GetMask("CBlock")))
        {
            if (Hitray.collider!=null)
            {
                Debug.DrawLine(ray.origin,Hitray.point);

                if (Hitray.collider.GetComponent<Renderer>() != null)
                {
                    mat = Hitray.collider.GetComponent<Renderer>().material;
                    mat.SetColor("_OutlineColor", OutlineColor);
                    mat.SetFloat("_Outline", OutlineWidth);
                    if ((Tmpobj.GetComponent<Renderer>()!=null)&&!Hitray.collider.gameObject.Equals(Tmpobj)) //레이 겹치는거 막고 한가지만 표시하기
                    {
                        Tmpobj.GetComponent<Renderer>().material.SetFloat("_Outline", 0f);
                    }
                    Tmpobj = Hitray.collider.gameObject;
                }
            }

        }
        // 충돌체가없다면 아웃라인 제거
        else 
        {
            if (Tmpobj.GetComponent<Renderer>() != null)
            {
                Tmpobj.GetComponent<Renderer>().material.SetFloat("_Outline", 0f);
            }

          //  Tmpobj = null;
        }
    }

    // 들기와 내리기는 블럭에 모든 스크립트를 넣기보단 행동하는 객체에서 작동하도록 스크립트 통합

    // 들기
    void Rift()
    {
        RiftingObject.transform.SetParent(RiftPosition);
        RiftingObject.transform.localPosition = RiftingObject.transform.localEulerAngles = Vector3.zero;
        RiftingObject.transform.GetComponent<BoxCollider>().enabled = false;
    }

    //내려놓기
    void Drop()
    {
        if (TranNum<3)
        {
            if (Tranparent[TranNum].GetComponent<TranparentBlock>().isDrop)
            {
                Tranparent[TranNum].GetComponent<TranparentBlock>().MeshReturn();
                RiftingObject.transform.SetParent(Tranparent[TranNum].GetComponent<TranparentBlock>().TargetBlock);
                RiftingObject.transform.localPosition = Tranparent[TranNum].GetComponent<TranparentBlock>().SelectPos;
                RiftingObject.transform.GetComponent<BoxCollider>().enabled = true;
                RiftingObject.transform.localEulerAngles = Vector3.zero;
                RiftingObject.transform.SetParent(null);

            }
            else
            {

            }
        }

    }
}
