﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SlotDesigner : MonoBehaviour {

    GameObject Canvas;
    [HideInInspector]
    public GameObject insobj;

    public GameObject Slot_Prefab;
    public string Canvas_Name; // 목표 갠버스의 이름
    public string SoltUI_Name; // 슬롯인벤토리의 이름
    public int[] TotalSize = new int[2];
    public int[] SlotSpace = new int[2];
    public int[] Slot_count = new int[2];


    public GridLayoutGroup.Corner Start_Corner;
    public GridLayoutGroup.Axis Start_Axis;
    public TextAnchor Child_Alignment=TextAnchor.MiddleCenter;


    // Use this for initialization
    public void CreateSlots()
    {
        GameObject Canvas = GameObject.Find(Canvas_Name);
        if(transform.FindChild(SoltUI_Name)==null)
        {
            insobj=Instantiate(new GameObject(SoltUI_Name), this.transform)as GameObject;
            insobj.transform.localPosition = Vector3.zero;
            insobj.name = SoltUI_Name;
        }
        else
        {
            insobj = transform.FindChild(SoltUI_Name).gameObject;
        }

        if (insobj.GetComponent<GridLayoutGroup>()==null)
        {
            insobj.gameObject.AddComponent<GridLayoutGroup>();
        }

    }

    public void BuildSlot(GameObject obj)
    {
       GridLayoutGroup Layout_group = obj.gameObject.GetComponent<GridLayoutGroup>(); 

        Layout_group.transform.GetComponent<RectTransform>().sizeDelta=new Vector2((float)TotalSize[0], (float)TotalSize[1]);

        /*
        1x1 = 1size 2space
        2x2 = 2size 3space
        3x3 = 3size 4space...
        (width-space*(row+1))/row = x
        */
        float slot_x = ((float)TotalSize[0]-((float)SlotSpace[0]*((float)Slot_count[0]+1f))) / (float)Slot_count[0];
        float slot_y = ((float)TotalSize[1] - ((float)SlotSpace[1] * ((float)Slot_count[1] + 1f))) / (float)Slot_count[1];

        Layout_group.cellSize = new Vector2(slot_x,slot_y);
        Layout_group.spacing = new Vector2((float)SlotSpace[0],(float)SlotSpace[0]);

        Layout_group.startCorner = Start_Corner;
        Layout_group.startAxis = Start_Axis;
        Layout_group.childAlignment = Child_Alignment;

        for (int i = 0; i < Slot_count[0]*Slot_count[1]; i++)
        {
            Instantiate(Slot_Prefab, obj.transform);
        }
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(SlotDesigner))]
public class SlotDesinerEditor : Editor
{
    SlotDesigner sltd;
    void OnEnable()
    {
        sltd = target as SlotDesigner;
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.BeginVertical("box");
        if (GUILayout.Button("Create"))
        {
            sltd.CreateSlots();
            sltd.BuildSlot(sltd.insobj);
        }
    }
}
#endif
