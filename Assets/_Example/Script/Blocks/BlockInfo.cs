﻿using UnityEngine;
using System.Collections;

public class BlockInfo : MonoBehaviour
{
    [HideInInspector]
    public static Vector3 SameSizePos = new Vector3(0f, 1f, 0f);
    [HideInInspector]
    public static Vector3[] LittleSizePosition=new Vector3[4];
    [HideInInspector]
    public static Vector3[] MoreLittleSizePosition = new Vector3[16];
    void Awake()
    {
        LittleSizePosition[0] = new Vector3(0.25f, 0.75f, 0.25f);
        LittleSizePosition[1] = new Vector3(0.25f, 0.75f, -0.25f);
        LittleSizePosition[2] = new Vector3(-0.25f, 0.75f, 0.25f);
        LittleSizePosition[3] = new Vector3(-0.25f, 0.75f, -0.25f);

        MoreLittleSizePosition[0] = new Vector3(0.125f, 0.625f, 0.125f);
        MoreLittleSizePosition[1] = new Vector3(-0.125f, 0.625f, 0.125f);
        MoreLittleSizePosition[2] = new Vector3(0.125f, 0.625f, -0.125f);
        MoreLittleSizePosition[3] = new Vector3(-0.125f, 0.625f, -0.125f);

        MoreLittleSizePosition[4] = new Vector3(0.375f, 0.625f, 0.375f);
        MoreLittleSizePosition[5] = new Vector3(0.375f, 0.625f, -0.375f);
        MoreLittleSizePosition[6] = new Vector3(-0.375f, 0.625f, 0.375f);
        MoreLittleSizePosition[7] = new Vector3(-0.375f, 0.625f, -0.375f);

        MoreLittleSizePosition[8] = new Vector3(0.375f, 0.625f, 0.125f);
        MoreLittleSizePosition[9] = new Vector3(0.375f, 0.625f, -0.125f);
        MoreLittleSizePosition[10] = new Vector3(-0.375f, 0.625f, 0.125f);
        MoreLittleSizePosition[11] = new Vector3(-0.375f, 0.625f, -0.125f);

        MoreLittleSizePosition[12] = new Vector3(0.125f, 0.625f, 0.375f);
        MoreLittleSizePosition[13] = new Vector3(0.125f, 0.625f, -0.375f);
        MoreLittleSizePosition[14] = new Vector3(-0.125f, 0.625f, 0.375f);
        MoreLittleSizePosition[15] = new Vector3(-0.125f, 0.625f, -0.375f);

    }
}
