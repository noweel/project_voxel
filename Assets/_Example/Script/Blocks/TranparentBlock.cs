﻿using UnityEngine;
using System.Collections;

public class TranparentBlock : MonoBehaviour {
    [Range(1,2)]
    public int Mysize;

    public float FitSize;
    //Transform OwnerTransform;
    public Transform MasterBlock;
    [HideInInspector]
    public bool isDrop;
    Renderer rend;
    public Transform[] ChkRayshotPos=new Transform[2];
    Ray Chkray;
    RaycastHit RayHit=new RaycastHit();
    public Transform myPointer;
    [HideInInspector]
    public Transform TargetBlock;
    Vector3 GoalPos;
    [HideInInspector]
    public Vector3 SelectPos;
	// Use this for initialization
	void Start ()
    {
        
        //OwnerTransform = GameObject.Find("VisionPos").transform;
        rend = GetComponentInChildren<Renderer>();
       // MyPos();


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckDropPosition();
        if (isDrop)
        {
            rend.material.SetColor("_Color", new Color(1f, 1f, 1f, 0.3f));
            MeshMyPos();
        }
        else
        {
            rend.material.SetColor("_Color", new Color(1f, 0f, 0f, 0.3f));
        }
    }


    Vector3 raypos;
    void CheckDropPosition()
    {
        raypos=ChkRayshotPos[1].position- ChkRayshotPos[0].position;
        Debug.DrawLine(ChkRayshotPos[1].position, ChkRayshotPos[0].position,Color.red);

        Chkray = new Ray(ChkRayshotPos[1].position, raypos);

        if (Physics.Raycast(Chkray,out RayHit,3f))
        {
            if (((RayHit.collider.CompareTag("Block")|| RayHit.collider.CompareTag("Block10")||RayHit.collider.CompareTag("Block05"))&&Mysize==1)||((RayHit.collider.CompareTag("Block") || RayHit.collider.CompareTag("Block10")) && Mysize == 2))
            {
                //isDrop = true;
                TargetBlock = RayHit.transform;
                myPointer.position = RayHit.point;
                myPointer.SetParent(TargetBlock);
                GoalPos = myPointer.localPosition;
                myPointer.SetParent(this.transform);
                DeterminPosition();

            }
            else
            {
                isDrop = false;
            }
        }
        else
        {
            isDrop = false;
        }
    }


    void DeterminPosition()
    {
        if (((RayHit.collider.CompareTag("Block10"))&&Mysize==2)||((RayHit.collider.CompareTag("Block05")) && Mysize == 1))
        {
            SelectPos = BlockInfo.SameSizePos;

        }
        else
        {
            if (((RayHit.collider.CompareTag("Block")) && Mysize == 2) || ((RayHit.collider.CompareTag("Block10")) && Mysize == 1))
            {
                float[] a = new float[4];
                for (int i = 0; i < a.Length; i++)
                {
                    a[i] = Vector3.Distance(GoalPos, BlockInfo.LittleSizePosition[i]);
                }
                for (int j = 0; j < a.Length; j++)
                {
                    if (Mathf.Min(a).Equals(a[j]))
                    {
                        SelectPos = BlockInfo.LittleSizePosition[j];
                    }
                }
            }
            else if (((RayHit.collider.CompareTag("Block")) && Mysize == 1))
            {
                float[] a = new float[16];
                for (int i = 0; i < a.Length; i++)
                {
                    a[i] = Vector3.Distance(GoalPos, BlockInfo.MoreLittleSizePosition[i]);
                }
                for (int j = 0; j < a.Length; j++)
                {
                    if (Mathf.Min(a).Equals(a[j]))
                    {
                        SelectPos = BlockInfo.MoreLittleSizePosition[j];
                    }
                }
            }

        }

    }
    public void MeshMyPos()
    {
        rend.transform.SetParent(TargetBlock);
        rend.transform.localPosition = SelectPos;
        rend.transform.localEulerAngles = Vector3.zero;
    }

    public void MeshReturn()
    {
        rend.transform.SetParent(this.transform);
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Block")|| coll.CompareTag("Block10")|| coll.CompareTag("Block05"))
        {
            Debug.Log("!!!1111");
            isDrop = false;
        }
    }
    void OnTriggerExit(Collider coll)
    {
        if (coll.CompareTag("Block") || coll.CompareTag("Block10") || coll.CompareTag("Block05"))
        {
            isDrop = true;
        }
    }
}
