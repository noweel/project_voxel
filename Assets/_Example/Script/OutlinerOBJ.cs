﻿using UnityEngine;
using System.Collections;

public class OutlinerOBJ : MonoBehaviour,RayInterface {


    //public Shader Noline;
    //public Shader Outline;
    //Shader shad;
    Material mat;
    public float OutlineWidth;
    public Color OutlineColor;
    void Start()
    {
        // Noline = Shader.Find("Custom/Nonoutline");
        // Outline = Shader.Find("Custom/simpleoutline");
        //GetComponent<Renderer>().material.shader = Noline;
        mat = GetComponent<Renderer>().material;
        mat.SetColor("_OutlineColor", OutlineColor);
        mat.SetFloat("_Outline", 0f);

    }

    public void Action()
    {
        // 쉐이더 프로퍼티는 마테리얼/setXXXXX("프로퍼티", 수치)로 설정 변경가능
        mat.SetFloat("_Outline", OutlineWidth);

        // if (shad!=Outline)
        // {
        //     GetComponent<Renderer>().material.shader = shad = Outline;
        // }
    }

    public void ActionEND()
    {
        mat.SetFloat("_Outline", 0f);
        
        // if (shad != Noline)
        // {
        //     GetComponent<Renderer>().material.shader = shad = Noline;
        // }
    }
}
