﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterInventory : MonoBehaviour
{
    public GameObject C_InvenObj;

    [HideInInspector]
    public Character_Info[] Array_InvenSlot;
    [HideInInspector]
    public List<Character_Info> C_InvenSlot=new List<Character_Info>();
    [HideInInspector]
    public List<Character_combiner> C_CombineSlot=new List<Character_combiner>();
    // Use this for initialization
    void Awake()
    {
        Array_InvenSlot= C_InvenObj.GetComponentsInChildren<Character_Info>();
        for (int i = 0; i < Array_InvenSlot.Length; i++)
        {
            C_InvenSlot.Add(Array_InvenSlot[i].GetComponent<Character_Info>());
            C_CombineSlot.Add(Array_InvenSlot[i].GetComponent<Character_combiner>());
        }
    }

    public void Insert_CI_data(Character_Info CInfo)
    {

        for (int i = 0; i < C_InvenSlot.Count; i++)
        {
            if(C_InvenSlot[i].isOn!=true)
            {
                C_InvenSlot[i].Syncroinfo(CInfo);
                Debug.Log(C_InvenSlot[i].isOn);
               // C_CombineSlot[i].Load_Char_Base();
               C_CombineSlot[i].Read_Char_info();
              My_Character_Info.My_Char_info[i] = C_InvenSlot[i];

                return;
            }
        }
    }

    public void Update_CI_data(Character_Info CInfo)
    {
        int tmpnum=-1;
        for (int i = 0; i < C_InvenSlot.Count; i++)
        {
            if (C_InvenSlot[i].Equals(C_InvenSlot.Find(x => x.CID.Equals(CInfo.CID))))
            {
                tmpnum = i;
            }
            if (tmpnum>=0)
            {
                C_InvenSlot[tmpnum] = CInfo;
                //C_CombineSlot[tmpnum].Read_Char_info();
                My_Character_Info.My_Char_info[tmpnum] = CInfo;
            }

        }
    }

    public static void Erase_CI_data(Character_Info CInfo)
    {

    }

}
