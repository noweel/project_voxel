﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class My_Character_Info : MonoBehaviour
{
    public static List<Character_Info> My_Char_info=new List<Character_Info>();
    public CharacterInventory CInven;
    void Start()
    {
        Load_Xml();
        Char_count_set();
      //  Refresh_Char_Allinven();
        isCanSaveChar();
    }

    public void Char_count_set()
    {
        Character_Info Tmp_new = new Character_Info();
        for (int i = 0; i < CInven.C_InvenSlot.Count; i++)
        {
            My_Char_info.Add(Tmp_new);
        }
        Debug.Log(My_Char_info.Count);
    }


    public bool isCanSaveChar()
    {
       Debug.Log(My_Char_info.FindAll(x=>x.isOn.Equals(false)).Count);

        if (My_Char_info.FindAll(x=>x.isOn.Equals(false)).Count >0)
        {
            return true;
        }
        else return false;
    }

    public void Refresh_Char_Allinven()
    {
        for (int i = 0; i < CInven.C_InvenSlot.Count; i++)
        {
            if (CInven.C_InvenSlot[i].isOn==false)
            {
                CInven.C_InvenSlot[i] = My_Char_info[i];
                CInven.C_CombineSlot[i].Read_Char_info();
            }
        }
    }

    public void Update_Char_In_Inven(Character_Info info)
    {
    }

    public void Update_Char_Part_item()
    {

    }

    public void Save_Xml()
    {

    }

    public void Load_Xml()
    {

    }
}


