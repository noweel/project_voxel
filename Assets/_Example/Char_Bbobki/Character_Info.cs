﻿using UnityEngine;
using System.Collections;



public class Character_Info : MonoBehaviour
{
    /*
    public GameObject headObject;               0
    public GameObject hairObject;               1
    public GameObject headAccesoriesObject;     2
    public GameObject rightShoulderObject;      3
    public GameObject rightElbowObject;         4
    public GameObject rightWeaponObject;        5
    public GameObject leftShoulderObject;       6
    public GameObject leftElbowObject;          7
    public GameObject leftWeaponObject;         8
    public GameObject leftShieldObject;         9
    public GameObject chestObject;              10
    public GameObject spineObject;              11
    public GameObject lowerSpineObject;         12
    public GameObject rightHipObject;           13
    public GameObject rightKneeObject;          14
    public GameObject leftHipObject;            15
    public GameObject leftKneeObject;           16

            ChangeHeadPart(Bbobki_combiner,headIndex);                  0
            ChangeHairPart(Bbobki_combiner, hairIndex);                 1
            ChangeChestPart(Bbobki_combiner, chestIndex);               2
            ChangeSpinePart(Bbobki_combiner, spineIndex);               3
            ChangeLowerSpinePart(Bbobki_combiner, lowerSpineIndex);     4
            ChangeArmParts(Bbobki_combiner, armPartsIndex);             5
            ChangeLegParts(Bbobki_combiner, legPartsIndex);             6
    */
    public int[] look_Value = new int[17];
    public int[] look_Base_Value = new int[7];
    public int[] info_Value;
    public bool isOn=false;
    public int CID;

    public void baseValueSync()
    {
        look_Value[0] = look_Base_Value[0];
        look_Value[1] = look_Base_Value[1];
        look_Value[2] = -1;
        look_Value[3] = look_Base_Value[5];
        look_Value[4] = look_Base_Value[5];
        look_Value[5] = -1;
        look_Value[6] = look_Base_Value[5];
        look_Value[7] = look_Base_Value[5];
        look_Value[8] = -1;
        look_Value[9] = -1;
        look_Value[10] = look_Base_Value[2];
        look_Value[11] = look_Base_Value[3];
        look_Value[12] = look_Base_Value[4];
        look_Value[13] = look_Base_Value[6];
        look_Value[14] = look_Base_Value[6];
        look_Value[15] = look_Base_Value[6];
        look_Value[16] = look_Base_Value[6];
    }
    public void Erase_value()
    {
        look_Value = new int[17];
        look_Base_Value = new int[7];
        isOn = false;
    }

    public void Syncroinfo(Character_Info info)
    {
        this.look_Value = info.look_Value;
        this.look_Base_Value = info.look_Base_Value;
        this.info_Value=info.info_Value;
        this.CID = info.CID;
        this.isOn = true;
    }
}

