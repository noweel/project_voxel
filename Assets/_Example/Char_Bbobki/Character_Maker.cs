﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character_Maker : MonoBehaviour
{
    public CharacterInventory CInven;

    #region 레어도 변수(등급)
    float[] normal_Rate;
    float[] high_Rate;
    float[] Rare_Rate;
    float[] S_Rate;
    #endregion
    #region 캐릭터 외형 커스터 마이징관련 변수
    public Character_combiner Bbobki_combiner;
    public PartsCollections collections;
    int headIndex;
    int hairIndex;
    int headAccesoriesIndex;
    int leftShoulderIndex;
    int leftElbowIndex;
    int leftWeaponIndex;
    int leftShieldIndex;
    int rightShoulderIndex;
    int rightElbowIndex;
    int rightWeaponIndex;
    int chestIndex;
    int spineIndex;
    int lowerSpineIndex;
    int leftHipIndex;
    int leftKneeIndex;
    int rightHipIndex;
    int rightKneeIndex;
    int armPartsIndex;
    int legPartsIndex;

    List<string> headKeys = new List<string>();
    List<string> hairKeys = new List<string>();
    List<string> headAccesoriesKeys = new List<string>();
    List<string> shoulderKeys = new List<string>();
    List<string> elbowKeys = new List<string>();
    List<string> weaponKeys = new List<string>();
    List<string> shieldKeys = new List<string>();
    List<string> chestKeys = new List<string>();
    List<string> spineKeys = new List<string>();
    List<string> lowerSpineKeys = new List<string>();
    List<string> hipKeys = new List<string>();
    List<string> kneeKeys = new List<string>();
    List<string> armPartsKeys = new List<string>();
    List<string> legPartsKeys = new List<string>();


    #endregion
    #region 캐릭터 능력치 생성 관련 변수
    #endregion


    // Use this for initialization
    void Start ()
    {
        
        Init(collections);
	}

    #region 등급 결정
    public int Selecet_Char_Grade(bool isRare)
    {
        int i = (isRare) ?0:1;
        int Result_grade;
        int randomvalue= Random.Range(0, 100);

        if (randomvalue>100f-S_Rate[i])
        {
            Result_grade =3;
        }
        else if (randomvalue > 100f - S_Rate[i]-Rare_Rate[i])
        {
            Result_grade =2;

        }
        else if (randomvalue > 100f - S_Rate[i] - Rare_Rate[i]-high_Rate[i])
        {
            Result_grade =1;

        }
        else 
        {
            Result_grade =0;

        }

        return Result_grade;
    }
    #endregion

    #region 캐릭터 파츠 정렬 참조및 키값을 List 정리

    public void Init(PartsCollections collections)
    {
        this.collections = collections;
        if (collections != null)
        {
            headKeys.AddRange(collections.headObjects.Keys);
            hairKeys.AddRange(collections.hairObjects.Keys);
            headAccesoriesKeys.AddRange(collections.headAccesoriesObjects.Keys);
            shoulderKeys.AddRange(collections.shoulderObjects.Keys);
            elbowKeys.AddRange(collections.elbowObjects.Keys);
            weaponKeys.AddRange(collections.weaponObjects.Keys);
            shieldKeys.AddRange(collections.shieldObjects.Keys);
            chestKeys.AddRange(collections.chestObjects.Keys);
            spineKeys.AddRange(collections.spineObjects.Keys);
            lowerSpineKeys.AddRange(collections.lowerSpineObjects.Keys);
            hipKeys.AddRange(collections.hipObjects.Keys);
            kneeKeys.AddRange(collections.kneeObjects.Keys);
            armPartsKeys.AddRange(collections.armParts.Keys);
            legPartsKeys.AddRange(collections.legParts.Keys);
            // Init neccesary parts
        }
    }
    #endregion

    public void Load_Char_All_Look(int[] AllLook, Character_combiner characterComb)
    {
        ChangeHeadPart(characterComb, AllLook[0]);
        ChangeHairPart(characterComb, AllLook[1]);
        ChangeHeadAccesoriesPart(characterComb, AllLook[2]);
        ChangeRightShoulderPart(characterComb, AllLook[3]);
        ChangeRightElbowPart(characterComb, AllLook[4]);
        ChangeRightWeaponPart(characterComb, AllLook[5]);
        ChangeLeftShoulderPart(characterComb, AllLook[6]);
        ChangeLeftElbowPart(characterComb, AllLook[7]);
        ChangeLeftWeaponPart(characterComb, AllLook[8]);
        ChangeLeftShieldPart(characterComb, AllLook[9]);
        ChangeChestPart(characterComb, AllLook[10]);
        ChangeSpinePart(characterComb, AllLook[11]);
        ChangeLowerSpinePart(characterComb, AllLook[12]);
        ChangeRightHipPart(characterComb, AllLook[13]);
        ChangeRightKneePart(characterComb, AllLook[14]);
        ChangeLeftHipPart(characterComb, AllLook[15]);
        ChangeLeftKneePart(characterComb, AllLook[16]);
    }
    public void Load_Char_Base_Look(Character_combiner characterComb,int[] BaseIndex)
    {
        ChangeHeadPart(characterComb, BaseIndex[0]);
        ChangeHairPart(characterComb, BaseIndex[1]);
        ChangeChestPart(characterComb, BaseIndex[2]);
        ChangeSpinePart(characterComb, BaseIndex[3]);
        ChangeLowerSpinePart(characterComb, BaseIndex[4]);
        ChangeArmParts(characterComb, BaseIndex[5]);
        ChangeLegParts(characterComb, BaseIndex[6]);
    }
    bool isFree;
    bool isMoney;
    bool isCanSlot;
    public int FreeBbobki;
    public int myMoney;
    public My_Character_Info MyCharInfo;

    public void isCanCheck()
    {
        if (FreeBbobki>0) isFree = true;
        else if (myMoney>= 0) isMoney = true;
        else isMoney = isFree = false;

        if (MyCharInfo.isCanSaveChar())isCanSlot = true;
        else isCanSlot = false;   
    }


    #region 캐릭터 외형 저장(유저정보의 Character_Info클래스의 변수형태로)

    public void Save_CharBase_Look(int[] Index_array)
    {
        Character_Info newInfo = new Character_Info();

        newInfo.CID = Random.Range(1, 999999999);
        while (My_Character_Info.My_Char_info.Find(x => x.CID.Equals(newInfo.CID)))
        {
            newInfo.CID = Random.Range(1, 999999999);
        }

        for (int i = 0; i < Index_array.Length; i++)
        {
            newInfo.look_Base_Value[i] = Index_array[i];
        }
        newInfo.baseValueSync();
        CInven.Insert_CI_data(newInfo);
    }


    #endregion
    #region 캐릭터 외형 커스터마이징(랜덤)
    [HideInInspector]
    public int cid; // 캐릭터 생성 횟수+캐릭터 아이디
    public void Randomize_Create()
    {
        isCanCheck();
        if ((isFree||isMoney)&&isCanSlot)
        {
            headIndex = Random.Range(0, headKeys.Count);
            hairIndex = Random.Range(0, hairKeys.Count);
            chestIndex = Random.Range(0, chestKeys.Count);
            spineIndex = Random.Range(0, spineKeys.Count);
            lowerSpineIndex = Random.Range(0, lowerSpineKeys.Count);
            armPartsIndex = Random.Range(0, armPartsKeys.Count);
            legPartsIndex = Random.Range(0, legPartsKeys.Count);

            int[] Random_Index = new int[] { headIndex, hairIndex, chestIndex, spineIndex, lowerSpineIndex, armPartsIndex, legPartsIndex };

            Load_Char_Base_Look(Bbobki_combiner, Random_Index);
            Save_CharBase_Look(Random_Index);

        }
        else
        {
            //생성할수없습니다 표시
        }
    }
    #endregion

    // 외형 교체의 특징 : 변수가 다른 함수 두종류(장비교체,해제)
    // 장비해제의  인덱스 변수는 -1
    #region 캐릭터 외형 교체(일반) 
    public void ChangeHeadPart(Character_combiner characterComb, int headIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[0] = headIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (headIndex < 0)
        {
            characterComb.ChangeHead();
            return;
        }
        characterComb.ChangeHead(collections.GetHead(headKeys[headIndex]));
    }

    public void ChangeHairPart(Character_combiner characterComb, int hairIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[1] = hairIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (hairIndex < 0)
        {
            characterComb.ChangeHair();
            return;
        }
        characterComb.ChangeHair(collections.GetHair(hairKeys[hairIndex]));
    }

    public void ChangeHeadAccesoriesPart(Character_combiner characterComb, int headAccesoriesIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[2] = headAccesoriesIndex;
       // CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (headAccesoriesIndex < 0)
        {
            characterComb.ChangeHeadAccesories();
            return;
        }
        characterComb.ChangeHeadAccesories(collections.GetHeadAccesories(headAccesoriesKeys[headAccesoriesIndex]));
    }

    public void ChangeChestPart(Character_combiner characterComb, int chestIndex)
    {
       // Debug.Log(chestIndex);
        characterComb.GetComponent<Character_Info>().look_Value[10] = chestIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (chestIndex < 0)
        {
            characterComb.ChangeChest();
            return;
        }
        characterComb.ChangeChest(collections.GetChest(chestKeys[chestIndex]));
    }

    public void ChangeSpinePart(Character_combiner characterComb, int spineIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[11] = spineIndex;
       // CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (spineIndex < 0)
        {
            characterComb.ChangeSpine();
            return;
        }
        characterComb.ChangeSpine(collections.GetSpine(spineKeys[spineIndex]));
    }

    public void ChangeLowerSpinePart(Character_combiner characterComb, int lowerSpineIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[12] = lowerSpineIndex;
        //CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (lowerSpineIndex < 0)
        {
            characterComb.ChangeLowerSpine();
            return;
        }
        characterComb.ChangeLowerSpine(collections.GetLowerSpine(lowerSpineKeys[lowerSpineIndex]));
    }

    public void ChangeArmParts(Character_combiner characterComb, int armPartsIndex)
    {
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (armPartsIndex < 0)
        {
            characterComb.ChangeArmParts();
            return;
        }
        characterComb.ChangeArmParts(collections.GetArmParts(armPartsKeys[armPartsIndex]));
    }

    public void ChangeLegParts(Character_combiner characterComb, int legPartsIndex)
    {
       // CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (legPartsIndex < 0)
        {
            characterComb.ChangeLegParts();
            return;
        }
        characterComb.ChangeLegParts(collections.GetLegParts(legPartsKeys[legPartsIndex]));
    }
    #endregion

    #region 캐릭터 외형 교체(세부) 장비에 따라 일부파츠만 바꿀필요가있을때를 위해

    public void ChangeLeftShoulderPart(Character_combiner characterComb, int leftShoulderIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[6] = leftShoulderIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (leftShoulderIndex < 0)
        {
            characterComb.ChangeLeftShoulder();
            return;
        }

        characterComb.ChangeLeftShoulder(collections.GetShoulder(shoulderKeys[leftShoulderIndex]));

    }

    public void ChangeLeftElbowPart(Character_combiner characterComb, int leftElbowIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[7] = leftElbowIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (leftElbowIndex < 0)
        {
            characterComb.ChangeLeftElbow();
            return;
        }
        characterComb.ChangeLeftElbow(collections.GetElbow(elbowKeys[leftElbowIndex]));
    }

    public void ChangeLeftWeaponPart(Character_combiner characterComb, int leftWeaponIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[8] = leftWeaponIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (leftWeaponIndex < 0)
        {
            characterComb.ChangeLeftWeapon();
            return;
        }
        characterComb.ChangeLeftWeapon(collections.GetWeapon(weaponKeys[leftWeaponIndex]));
    }

    public void ChangeLeftShieldPart(Character_combiner characterComb, int leftShieldIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[9] = leftShieldIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (leftShieldIndex < 0)
        {
            characterComb.ChangeLeftShield();
            return;
        }
        characterComb.ChangeLeftShield(collections.GetShield(shieldKeys[leftShieldIndex]));
    }

    public void ChangeRightShoulderPart(Character_combiner characterComb, int rightShoulderIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[3] = rightShoulderIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (rightShoulderIndex < 0)
        {
            characterComb.ChangeRightShoulder();
            return;
        }
        characterComb.ChangeRightShoulder(collections.GetShoulder(shoulderKeys[rightShoulderIndex]));
    }

    public void ChangeRightElbowPart(Character_combiner characterComb, int rightElbowIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[4] = rightElbowIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (rightElbowIndex < 0)
        {
            characterComb.ChangeRightElbow();
            return;
        }
        characterComb.ChangeRightElbow(collections.GetElbow(elbowKeys[rightElbowIndex]));
    }

    public void ChangeRightWeaponPart(Character_combiner characterComb, int rightWeaponIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[5] = rightWeaponIndex;
     //   CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (rightWeaponIndex < 0)
        {
            characterComb.ChangeRightWeapon();
            return;
        }
        characterComb.ChangeRightWeapon(collections.GetWeapon(weaponKeys[rightWeaponIndex]));
    }

    public void ChangeLeftHipPart(Character_combiner characterComb, int leftHipIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[15] = leftHipIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (leftHipIndex < 0)
        {
            characterComb.ChangeLeftHip();
            return;
        }
        characterComb.ChangeLeftHip(collections.GetHip(hipKeys[leftHipIndex]));
    }

    public void ChangeLeftKneePart(Character_combiner characterComb, int leftKneeIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[16] = leftKneeIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (leftKneeIndex < 0)
        {
            characterComb.ChangeLeftKnee();
            return;
        }
        characterComb.ChangeLeftKnee(collections.GetKnee(kneeKeys[leftKneeIndex]));
    }

    public void ChangeRightHipPart(Character_combiner characterComb, int rightHipIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[13] = rightHipIndex;
     //   CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (rightHipIndex < 0)
        {
            characterComb.ChangeRightHip();
            return;
        }
        characterComb.ChangeRightHip(collections.GetHip(hipKeys[rightHipIndex]));
    }

    public void ChangeRightKneePart(Character_combiner characterComb, int rightKneeIndex)
    {
        characterComb.GetComponent<Character_Info>().look_Value[14] = rightKneeIndex;
      //  CInven.Update_CI_data(characterComb.GetComponent<Character_Info>());

        if (rightKneeIndex < 0)
        {
            characterComb.ChangeRightKnee();
            return;
        }
        characterComb.ChangeRightKnee(collections.GetKnee(kneeKeys[rightKneeIndex]));
    }
    #endregion
}
