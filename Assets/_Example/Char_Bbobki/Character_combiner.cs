﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character_combiner : MonoBehaviour {

    public GameObject headObject;           //0
    public GameObject hairObject;           //1
    public GameObject headAccesoriesObject; //2
    public GameObject rightShoulderObject;  //3
    public GameObject rightElbowObject;     //4
    public GameObject rightWeaponObject;    //5
    public GameObject leftShoulderObject;   //6
    public GameObject leftElbowObject;      //7
    public GameObject leftWeaponObject;     //8
    public GameObject leftShieldObject;     //9
    public GameObject chestObject;          //10
    public GameObject spineObject;          //11
    public GameObject lowerSpineObject;     //12
    public GameObject rightHipObject;       //13
    public GameObject rightKneeObject;      //14
    public GameObject leftHipObject;        //15
    public GameObject leftKneeObject;       //16

    public GameObject[] char_Parts; // 파츠를 배열화 시켜 넘버링으로 찾을수있게끔 해주는 변수
     
    public bool rightSideScaleXMinusOne; // 오른쪽의 스케일값을 변경할것인가 여부
    public bool leftSideScaleXMinusOne;  // 왼쪽의 스케일 값을 변경할것인가
    public Character_Maker Char_maker;
    public Character_Info C_info;
    public My_Character_Info MC_info;
    // Use this for initialization
    void Start ()
    {
    }

    public void Read_Char_info()
    {
        if (C_info.isOn)
        {
            Char_maker.Load_Char_All_Look(C_info.look_Value, this);
        }
    }
    public void Load_Char_Base()
    {
       // Debug.Log(C_info.look_Base_Value[2]);
        Char_maker.Load_Char_Base_Look(this, C_info.look_Base_Value);
    }
    public void Erase_Char()
    {
        for (int i = 0; i < char_Parts.Length; i++)
        {
            char_Parts[i].SetActive(false);
        }
        C_info.isOn = false;
    }

    public void ChangeObject(int PartNum, GameObject changer)
    {
        char_Parts[PartNum].SetActive(true);
        char_Parts[PartNum].GetComponent<MeshFilter>().mesh = changer.GetComponent<MeshFilter>().sharedMesh;
        char_Parts[PartNum].GetComponent<Renderer>().material = changer.GetComponent<Renderer>().sharedMaterial;
    }

    public void ChangeObject(GameObject parts, GameObject changer)
    {
        parts.SetActive(true);
        parts.GetComponent<MeshFilter>().mesh = changer.GetComponent<MeshFilter>().sharedMesh;
        parts.GetComponent<Renderer>().material = changer.GetComponent<Renderer>().sharedMaterial;
    }

    public void ChangeObject(GameObject parts)
    {
        parts.SetActive(false);
    }


    public void ChangeHead(GameObject obj)
    {
        ChangeObject(headObject, obj);
    }
    public void ChangeHead()
    {
        ChangeObject(headObject);
    }

    public void ChangeHair(GameObject obj)
    {
        ChangeObject(hairObject, obj);
    }
    public void ChangeHair()
    {
        ChangeObject(hairObject);
    }
    public void ChangeHeadAccesories(GameObject obj)
    {
        ChangeObject(headAccesoriesObject, obj);
    }
    public void ChangeHeadAccesories()
    {
        ChangeObject(headAccesoriesObject);
    }

    public void ChangeRightShoulder(GameObject obj)
    {
        ChangeObject(rightShoulderObject, obj);
        /*
        if (rightSideScaleXMinusOne && rightShoulderObject != null)
        {
            Vector3 newScale = rightShoulderObject.transform.localScale;
            newScale.x *= -1;
            rightShoulderObject.transform.localScale = newScale;
        }
        */
    }
    public void ChangeRightShoulder()
    {
        ChangeObject(rightShoulderObject);
    }

    public void ChangeRightElbow(GameObject obj)
    {
        ChangeObject(rightElbowObject, obj);
        /*
        if (rightSideScaleXMinusOne && rightElbowObject != null)
        {
            Vector3 newScale = rightElbowObject.transform.localScale;
            newScale.x *= -1;
            rightElbowObject.transform.localScale = newScale;
        }
        */
    }
    public void ChangeRightElbow()
    {
        ChangeObject(rightElbowObject);
    }

    public void ChangeRightWeapon(GameObject obj)
    {
        ChangeObject(rightWeaponObject, obj);
    }
    public void ChangeRightWeapon()
    {
        ChangeObject(rightWeaponObject);
    }
    public void ChangeLeftShoulder(GameObject obj)
    {
        ChangeObject(leftShoulderObject, obj);
        /*
        if (leftSideScaleXMinusOne && leftShoulderObject != null)
        {
            Vector3 newScale = leftShoulderObject.transform.localScale;
            newScale.x *= -1;
            leftShoulderObject.transform.localScale = newScale;
        }
        */
    }
    public void ChangeLeftShoulder()
    {
        ChangeObject(leftShoulderObject);
    }

    public void ChangeLeftElbow(GameObject obj)
    {
        ChangeObject(leftElbowObject, obj);
        /*
        if (leftSideScaleXMinusOne && leftElbowObject != null)
        {
            Vector3 newScale = leftElbowObject.transform.localScale;
            newScale.x *= -1;
            leftElbowObject.transform.localScale = newScale;
        }
        */
    }
    public void ChangeLeftElbow()
    {
        ChangeObject(leftElbowObject);
    }

    public void ChangeLeftWeapon(GameObject obj)
    {
        ChangeObject(leftWeaponObject, obj);
    }
    public void ChangeLeftWeapon()
    {
        ChangeObject(leftWeaponObject);
    }

    public void ChangeLeftShield(GameObject obj)
    {
        ChangeObject(leftShieldObject, obj);
    }
    public void ChangeLeftShield()
    {
        ChangeObject(leftShieldObject);
    }
    public void ChangeChest(GameObject obj)
    {
        ChangeObject(chestObject, obj);
    }
    public void ChangeChest()
    {
        ChangeObject(chestObject);
    }
    public void ChangeSpine(GameObject obj)
    {
        ChangeObject(spineObject, obj);
    }
    public void ChangeSpine()
    {
        ChangeObject(spineObject);
    }
    public void ChangeLowerSpine(GameObject obj)
    {
        ChangeObject(lowerSpineObject, obj);
    }
    public void ChangeLowerSpine()
    {
        ChangeObject(lowerSpineObject);
    }
    public void ChangeRightHip(GameObject obj)
    {
        ChangeObject(rightHipObject, obj);
        if (rightSideScaleXMinusOne && rightHipObject != null)
        {
            Vector3 newScale = rightHipObject.transform.localScale;
            newScale.x *= -1;
            rightHipObject.transform.localScale = newScale;
        }
    }
    public void ChangeRightHip( )
    {
        ChangeObject(rightHipObject);
    }

    public void ChangeRightKnee(GameObject obj)
    {
        ChangeObject(rightKneeObject, obj);
        if (rightSideScaleXMinusOne && rightKneeObject != null)
        {
            Vector3 newScale = rightKneeObject.transform.localScale;
            newScale.x *= -1;
            rightKneeObject.transform.localScale = newScale;
        }
    }
    public void ChangeRightKnee( )
    {
        ChangeObject(rightKneeObject);

    }
    public void ChangeLeftHip(GameObject obj)
    {
        ChangeObject(leftHipObject, obj);
        if (leftSideScaleXMinusOne && leftHipObject != null)
        {
            Vector3 newScale = leftHipObject.transform.localScale;
            newScale.x *= -1;
            leftHipObject.transform.localScale = newScale;
        }
    }
    public void ChangeLeftHip()
    {
        ChangeObject(leftHipObject);
    }
    public void ChangeLeftKnee(GameObject obj)
    {
        ChangeObject(leftKneeObject, obj);
        if (leftSideScaleXMinusOne && leftKneeObject != null)
        {
            Vector3 newScale = leftKneeObject.transform.localScale;
            newScale.x *= -1;
            leftKneeObject.transform.localScale = newScale;
        }
    }
    public void ChangeLeftKnee()
    {
        ChangeObject(leftKneeObject);

    }
    public void ChangeArmParts(ArmParts parts)
    {
        ChangeLeftShoulder(parts.shoulder);
        ChangeLeftElbow(parts.elbow);
        ChangeRightShoulder(parts.shoulder);
        ChangeRightElbow(parts.elbow);
    }
    public void ChangeArmParts()
    {
        ChangeLeftShoulder();
        ChangeLeftElbow();
        ChangeRightShoulder();
        ChangeRightElbow();
    }

    public void ChangeLegParts(LegParts parts)
    {
        ChangeLeftHip(parts.hip);
        ChangeLeftKnee(parts.knee);
        ChangeRightHip(parts.hip);
        ChangeRightKnee(parts.knee);
    }
    public void ChangeLegParts()
    {
        ChangeLeftHip();
        ChangeLeftKnee();
        ChangeRightHip();
        ChangeRightKnee();
    }
}
