﻿//유니티는 쉐이더 랩이라는 언어로 래핑되며 쉐이더를 구성하는데 사용
// surface // vertex and fragment // fixed function shader // 로 작성가능하다
// 이 쉐이더 프로그램은 Cg/HLSL 언어로 작성하게 된다 
// Cg (C for Graphic) C언어를 기반한 세이딩 언어 (엔비디아 개발)
// HLSL(high level shader language) 마소에서 개발한 표준 다이렉트3D API에 사용되는 세이딩 언어 이며  Cg와 유사 

//쉐이더의 기본 문법
//Shader "name" { [Properties] Subshaders [Fallback] [CustomEditor] } 

Shader "Custom/MDiffuseOutline"  // 커스텀 항목의 심플아웃라인이라는 이름으로 정의
{
	Properties //쉐이더의 옵션부분으로 머터리얼 인스펙터의 설정에 표시되는 부분 
	{
		//_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		//_Glossiness ("Smoothness", Range(0,1)) = 0.5
		//_Metallic ("Metallic", Range(0,1)) = 0.0
		_Outline("Outline", Range(0.0, 0.01)) = 0.003
		_OutlineColor("Outline Color", Color) = (0,0,1,1)
	}
		/*
		//유니티쉐이더를 구성하는 실제 부분 서브쉐이더가 없을경우  fallback을 이용함
		//Unity에서 메쉬를 그릴 때, 사용할 쉐이더를 찾아 사용자의 그래픽 카드에서 실행하는 첫 번째 서브 쉐이더를 선택합니다.
		//
		//Subshader { [Tags] [CommonState] Passdef [Passdef ...] }
		// 서브쉐이더 구성 Tag는 선택적으로표시할수 있며 표준상태(CommonState) ,passdef(패스의 정의 목록)과 정의함
		//
		//Unity가 렌더링하는 서브 쉐이더를 선택할 때, 패스마다(빛의 상호 작용에 따라 더 많은) 오브젝트를 한 번 렌더링함. 결국 이 패스가 많을 수록 렌더링 패스가 증가
		//오브젝트의 렌더링은 고가의 처리이기 때문에 쉐이더를 가능한 한 최소의 양의 패스를 정의 할 필요가 있음
		//BUT 그래픽 하드웨어에 따라 여러 패스를 사용할 수 밖에 없는 경우가 존재
		*/
	SubShader
		{
		Tags { "RenderType"="Opaque" } // render type의 이름으로 opaque(렌더러 타입의 기본으로 일반 솔리드 오브젝트에 적합)의 값으로 설정
		LOD 200 //쉐이더LOD로 (shader level of detail) 정해진 LOD값의 이하인경우 해당 (서브)쉐이더를 사용 100~600사이에 형성되며 Diffuse의경우200
		
		CGPROGRAM //표면쉐이더의 표현을 시작( Cg / HLSL에서 스니펫(토막화))으로 ENDCG로 끝난다. CGPROGRAM~ENDCG사이에 표면쉐이더 문법 구현

		#pragma surface surf Lambert noforwardadd
			//#progma surface 로 표면(서페이스)쉐이더에 대한 설정을 나타냄
			//surf Standard fullforwardshadows 표준 광원효과로 모든종류의 광원과 그림자에 사용

		// 쉐이더 모델버전 3.0으로 설정(유니티5버전 기본 표면쉐이더기준)으로 컴파일
		#pragma target 3.0


		struct Input 
		{
			float2 uv_MainTex; // 텍스처의 시작 좌표 float2 -> x,y 
			//ex) float4 -> (0f,0f,0f,0f)   /// float3x2 = {(0f)(0f)},{(0f)(0f)},{(0f)(0f)}
		};

		//half _Glossiness;
		//half _Metallic;
		// half 바이너리16또는 반정밀부동소수점형식으로 부동소수점 정밀률이 32비트인 float의 절반

		//fixed4 _Color;
		//Cg레퍼런스에서 발췌 fixed S1.10 fixed point, clamping to[-2, 2) -2~2사이의 10트 표현 방식으로 이해했음 아닐지도...ㅠㅠ
		// 색상 프로퍼티를 참조하기 위해 선언하는 단계로 색표현(0f,0f,0f,0f)시 fixed4로 값을 지정함

		sampler2D _MainTex;
		// 2d텍스처에 프로퍼티에 접근하기 위해 선언

		// 쉐이더랩 프로퍼티 유형과 Cg/HLSL변수유형과의 대응 관계
		//색상과 3차원좌표값은 float4, half4 or fixed4으로
		//거리관계 또는 Float값은 float, half, fixed 
		//Texture 프로퍼티는 일반적으로(2D) 텍스처의 경우 sampler2D 변수에 대응, 3D 텍스처의 경우 sampler3D 변수에 대응

		void surf (Input IN, inout SurfaceOutput o) //surface shader에 대한 메소드
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex); 
			//텍스처종류와 시작좌표에 컬러값을 구함
			o.Albedo = c.rgb; 
			//surface쉐이더의 알베도 값
			// Metallic and smoothness come from slider variables
			//o.Metallic = _Metallic; 
			// 금속성질감 정도
			//o.Smoothness = _Glossiness; 
			//윤기?정도
			o.Alpha = c.a; //텍스처에따른 알파값
		}
		ENDCG //surface shader 끝

			// 패스는 3종으로 regular pass , use pass,grab pass으로 구분
			//regular pass
			//가장 일반적인 패스로  => Pass { [Name and Tags] [RenderSetup] }의 구조를 가지고 있음
			// [Name and Tags] -> Pass를 지칭하여 렌더링 엔진에 전달하는 Name/Value 문자열로 정의가 가능(생략도 가능하고 태그의 경우 중복하여 설정도 가능)
			// [RenderSetup] -> 그래픽 하드웨어 State(상태)를 설정합니다
			//usepass
			// usepass "shader/name"으로 표현하며 주어진 쉐이더에서 이름을 가진 패스를 사용할수 있음 불러오는 대상 쉐이더의 처음 서브쉐이더에서만 고려됨
			//grab pass
			//특별한 패스타입으로, 렌더링될때 화면의 내용을  텍스처에 잡아둠(그랩)으로 고급 이미지 기반효과에 사용될수 있음
			// 사용법으로 grabpass{}로 전체를 텍스쳐에 잡아두고 _GrabTexture로 불러오는 방법(비용이 비쌈)
			// grabpass{"텍스쳐 이름"}으로 프레임당 1회 텍스쳐이름을 사용하는 첫번쨰 오브젝트만 잡아두고 후속패스로 지정된 텍스처 이름으로 불러올수 있음

		pass //패스를 시작
		{
			Cull front  //컬링은 시점과 관련하여 렌더링에대한 최적화 옵션
						// Cull back 시점반대편을 렌더링하지 않음 // cull front 시점방향면을 렌더링하지않음 // cull off 컬링을 실행하지않음(전체 렌더링)

			//  ZWrite On/OFF
			//  오브젝트의 픽셀 깊이 버퍼에 대한 제어(기본값 on)-> 불투명한 오브젝트 off의 경우 부분부분 투명효과를 그릴때 사용

			//  ZTest less/greater/LEqual/GEqual/Equal/NotEqual/Always
			//  깊이 테스트 실행방법으로 LEqual(기본값으로 이미 그려진 오브젝트와 거리가 같거나 더 가까운 경우에 그리고 먼 경우 오브젝트로 숨김)

			// Offset factor, Units
				// 깊이의 오프셋의 파라미터를 펙터와 유닛으로 지정하는데 펙터는 폴리곤의 z기울기로, 유니트는 최소깊이 버퍼값으로 스케일 (뭔소리냐..ㅠㅠ)
				CGPROGRAM


			#pragma vertex vert //정점(vertex)쉐이더로 정의되는  vert의 함수
			#pragma fragment frag //Fragment 쉐이더를 frag로 정의
			#include "UnityCG.cginc" //내장 쉐이더를 포함시켜 미리 정의된 변수나 헬퍼함수를 가지게 하는것

				//HLSLSupport.cginc
				//이 파일은 쉐이더를 컴파일할 때 _자동으로 포함_됩니다.멀티 - 플랫폼 쉐이더 개발을 돕기 위해, 다양한 preprocessor macros를 정의합니다.

				//UnityShaderVariables.cginc
				//이 파일은 쉐이더를 컴파일할 때 _자동으로 포함_됩니다.멀티 플랫폼 쉐이더 개발을 돕기 위해, 다양한 preprocessor macros를 정의합니다.

				//UnityCG.cginc
				//이 파일은 많은 헬퍼 함수를 가져오기 위해서 Unity 쉐이더에 자주 포함됩니다.
				//UnityCG.cginc 데이터 구조
				//struct appdata_base : 위치, 법선, 텍스처 좌표, 정점 쉐이더 입력.
				//struct appdata_tan : 위치, 법선, 접선, 한 개의 텍스처 좌표의 정점 쉐이더 입력.
				//struct appdata_full : 위치, 법선, 접선, 정점 색상, 두 개의 텍스처 좌표의 정점 쉐이더 입력.
				//struct appdata_img : 위치, 한 개의 텍스처 좌표의 정점 쉐이더 입력.

				//-함수
				//UnityWorldSpaceViewDir(worldPos) : 월드 포지션으로 뷰 벡터를 구한다.
				//UnityObjectToWorldNormal(i.normal) : 노말을 월드 노말로
				//UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, i.worldRefl) : 디폴트 리플렉션 큐브맵을 뽑는다.
				//half4 skyData = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, i.worldRefl);


				float _Outline; // 아웃라인 굵기
			uniform float4 _OutlineColor; //읽기 전용(uniform)으로 설정된 아웃라인 색상


			//v2f 는 vertex to fragment, 정점 셰이더에서 프래그먼트(픽셀) 셰이더로 넘어가는 데이터이며 정점 쉐이더의 출력을 위한 구조체

			struct v2f   
			{
				float4 pos : SV_POSITION; //픽셀의 공간위치
				float3 color : COLOR0; // 색상
			};

			// 아웃라인을 그리는 방법으로
			// 정점의 법선 벡터 방향으로 모델을 확대시켜 한번 더 렌더링하도록 하는 방식

			v2f vert(appdata_base v) // 정점쉐이더
			{
				v2f o; //v2f=o

				//정점의 공간 변환
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex); // v2f의 위치는 뮬연산(곱)을 통해 appdata_base의 버텍스를 스크린 화면의 포지션 값

				//법선 벡터 공간 변환
				float3 norm = normalize(mul((float3x3)UNITY_MATRIX_IT_MV, v.normal));
				float2 offset = TransformViewToProjection(norm.xy);
				//정점의 법선벡터(v.normal)에 역 전치변환 행렬을 곱하고 다시 투영행렬을 곱하는 과정...=_=.....
				//로컬 공간에서 정의된 법선을 투영 공간(Projection space)으로 변환하는 코드라고한다....=_=....
				//mul이 아닌 TransformViewToProjection을 통해 투영행렬을 통해 호환성극복(모바일).... =_=... 아놔

				//변수
				//UNITY_MATRIX_MVP : 유니티에서 제공하는 내장 변환 행렬의 하나(모델, 뷰, 투영)으로 vertex와 곱하면 스크린 공간에서의 위치값을 알수있음
				//UNITY_MATRIX_IT_MV : 모델-뷰 역 전치 변환행렬( Model-View Inverse Transpose )로 유니티에서 내장 제공하는 내장 변환 행렬
				//_Object2World : vertex와 곱하면 월드 스페이스의 포지션

				o.pos.xy += offset*o.pos.z*_Outline;
				// 최종 정점을 구하는 식으로 확대 정도는 정점의 z 값과(카메라 공간에서의)_Outline(외곽선 굵기 정도)에 비례한다.
					
				o.color= _OutlineColor;
				// 색상은 프로퍼티에 설정된 색

				UNITY_TRANSFER_FOG(o, o.pos);
				// 포그 기능의 좌표를 저장
				return o;
				
			}

			half4 frag(v2f i) : COLOR //fragment shader출력으로  half4형식의 색상을 반환
			{
				return half4(_OutlineColor);
			}
				ENDCG
		}
	}
	FallBack "Mobile/VertexLit" //모든 서브쉐이더를 시도후 폴백을 시도하라는 의미로 실행할 하위쉐이더가 없는 경우 Diffuse쉐이더를 실행
}
