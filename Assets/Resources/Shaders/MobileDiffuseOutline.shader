﻿
Shader "Custom/Mobile_OutLine"  // 커스텀 항목의 심플아웃라인이라는 이름으로 정의
{
	Properties //쉐이더의 옵션부분으로 머터리얼 인스펙터의 설정에 표시되는 부분 
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		[NoScaleOffset] _BumpMap("Normalmap", 2D) = "bump" {}

		_Outline("Outline", Range(0.0, 0.01)) = 0.003
		_OutlineColor("Outline Color", Color) = (0,0,1,1)
	}

	SubShader
		{
		Tags { "RenderType"="Opaque" } 
		LOD 200 
		CGPROGRAM 

		#pragma surface surf Lambert noforwardadd
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpMap;

		struct Input 
		{
			float2 uv_MainTex; // 텍스처의 시작 좌표 float2 -> x,y 
			//ex) float4 -> (0f,0f,0f,0f)   /// float3x2 = {(0f)(0f)},{(0f)(0f)},{(0f)(0f)}
		};

		void surf (Input IN, inout SurfaceOutput o) //surface shader에 대한 메소드
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex); 
			//텍스처종류와 시작좌표에 컬러값을 구함
			o.Albedo = c.rgb; 

			o.Alpha = c.a; //텍스처에따른 알파값
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex)); //노말맵 적용

		}
		ENDCG //surface shader 끝

			pass //패스를 시작
		{
			Cull front  //컬링은 시점과 관련하여 렌더링에대한 최적화 옵션

				CGPROGRAM


#pragma vertex vert //정점(vertex)쉐이더로 정의되는  vert의 함수
#pragma fragment frag //Fragment 쉐이더를 frag로 정의
#include "UnityCG.cginc" //내장 쉐이더를 포함시켜 미리 정의된 변수나 헬퍼함수를 가지게 하는것


			float _Outline; // 아웃라인 굵기
			uniform float4 _OutlineColor; //읽기 전용(uniform)으로 설정된 아웃라인 색상


			struct v2f
			{
				float4 pos : SV_POSITION; //픽셀의 공간위치
				float3 color : COLOR0; // 색상
			};

			v2f vert(appdata_base v) // 정점쉐이더
			{
				v2f o; //v2f=o

				//정점의 공간 변환
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex); // v2f의 위치는 뮬연산(곱)을 통해 appdata_base의 버텍스를 스크린 화면의 포지션 값

				//법선 벡터 공간 변환(핵심)
				float3 norm = normalize(mul((float3x3)UNITY_MATRIX_IT_MV, v.normal));
				float2 offset = TransformViewToProjection(norm.xy);


				o.pos.xy += offset*o.pos.z*_Outline;
				// 최종 정점을 구하는 식으로 확대 정도는 정점의 z 값과(카메라 공간에서의)_Outline(외곽선 굵기 정도)에 비례한다.

				o.color = _OutlineColor;
				// 색상은 프로퍼티에 설정된 색

				UNITY_TRANSFER_FOG(o, o.pos);
				// 포그 기능의 좌표를 저장
				return o;

			}

			half4 frag(v2f i) : COLOR //fragment shader출력으로  half4형식의 색상을 반환
			{
				return half4(_OutlineColor);
			}

				ENDCG
		}
	}
	FallBack "Mobile/Diffuse" //모든 서브쉐이더를 시도후 폴백을 시도하라는 의미로 실행할 하위쉐이더가 없는 경우 Diffuse쉐이더를 실행
}
